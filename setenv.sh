#!/bin/sh

# IBM DB2
LD_LIBRARY_PATH=/home/mark/ibm/dsdriver/lib

# GCC 10.1.0
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib64

# OPEN SSL
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/mark/devel/lib/openssl/lib

export LD_LIBRARY_PATH


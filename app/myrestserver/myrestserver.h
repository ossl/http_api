#ifndef MY_REST_SERVER_H
#define MY_REST_SERVER_H

#include "logger/loggerfactory.h"
#include "logger/logger.h"

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

#include <memory>

class MyRestServer //: public std::enable_shared_from_this<MyRestServer>
{
public:
    MyRestServer();
    
    void start( std::shared_ptr<Opal::LoggerFactory> loggerFactory,
                boost::asio::io_service &io_service,
                boost::asio::ip::tcp::endpoint endpoint,
                boost::asio::ssl::context *ssl_context = nullptr );

};

#endif

#include "db2requestcallsp.h"

Db2RequestCallSP::Db2RequestCallSP(Opal::SQL::SqlCall &&call) :
    Db2Request(), call(std::move(call))
{}

void Db2RequestCallSP::exec(
    Opal::Database::Db2Connection &conn,
    Opal::SQL::SqlResultData &sqlResultData) const
{
    auto stat = conn.createStatement();

    execSQLDirect(stat, sqlResultData, buildSql(call));
}

std::string Db2RequestCallSP::buildSql( const Opal::SQL::SqlCall &sqlCall ) const
{
    std::string seperator;
    std::string sql = "CALL ";

    sql += sqlCall.stored_proc + "(";
    sql += Opal::algo::flatten(sqlCall.params, ", ");
    sql += ")";

    return sql;
}

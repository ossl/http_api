#ifndef DB2_REQUEST_H
#define DB2_REQUEST_H

#include "database/db2/db2connection.h"
#include "database/db2/db2statement.h"
#include "sql/sqlresultdata.h"
#include <string>
#include <assert.h>

class Db2Request
{
public:
    virtual ~Db2Request() {}
    virtual void exec(
        Opal::Database::Db2Connection &conn,
        Opal::SQL::SqlResultData &sqlResultData) const
    {
        assert(false);
    }

protected:
    void execSQLDirect(
        Opal::Database::Db2Statement &stat,
        Opal::SQL::SqlResultData &sqlResultData,
        const std::string &sql) const
    {
    auto rset = stat.query(sql);
    auto numCols = rset.numCols();

    sqlResultData.columns = rset.columnNames();

    while (rset.next())
    {
        std::vector<std::string> row(numCols);

        for (size_t i = 1; i <= numCols; ++i)
        {
            row[i - 1] = rset.data(i);
        }

        sqlResultData.rows.push_back(std::move(row));
    }
}

};

#endif

///
//
// TODO: Perhaps add option for persisting connection for a period
//       of time, though this can only be valid if the request is
//       for the same User.
//
// TODO: Use a thread pool, rather than creating a new thread for
//       each connection.
//

#ifndef DB2_CLIENT_H
#define DB2_CLIENT_H

#include "sql/sqlresultdata.h"
#include "sql/sqlquery.h"
#include "sql/sqlcall.h"

#include "db2request.h"

#include <boost/asio.hpp>

#include <functional>
#include <memory>
#include <thread>
#include <list>

class Db2Client : public std::enable_shared_from_this<Db2Client>
{
public:
    Db2Client( boost::asio::io_service &io_service,
               std::string &&ipAddress,
               int port,
               std::string &&dbName ) : 
        io_service(io_service),
        ipAddress(std::move(ipAddress)),
        port(port),
        dbName(std::move(dbName))
    {}

    template <class Handler>
    void async_exec (
        const std::string &username,
        const std::string &password,
        std::unique_ptr<Db2Request> &&request,
        Handler &&handler)
    {
        auto t = std::thread(
            [this, self = shared_from_this(), username, password, request{move(request)}, handler]
            {
                Opal::SQL::SqlResultData sqlResultData =
                    exec(username, password, request);

                // Post completion to ASIO service, which will call the
                // response function in the main thread.

                io_service.post(
                    [this, self, sqlResultData{std::move(sqlResultData)}, handler]
                    {
                        // Can't move from captured var in call to handler
                        // for some reason, so move to local copy of string
                        // first [investigate].

                        Opal::SQL::SqlResultData localResult = std::move(sqlResultData);

                        handler(std::move(localResult));
                
                        clearCompletedWorkerThreads();
                    });
            });
        workers.push_back(std::move(t));        
    }

    Opal::SQL::SqlResultData exec(
        const std::string &username,
        const std::string &password,
        const std::unique_ptr<Db2Request> &request)
    {
        Opal::SQL::SqlResultData sqlResultData;

        try
        {
            Opal::Database::Db2Connection conn(
                ipAddress, port, username, password, dbName);

            request->exec(conn, sqlResultData);
        }
        catch (Opal::Database::DriverException &e)
        {
            sqlResultData.isException = true;
            sqlResultData.errorMessage = e.what();
            sqlResultData.sqlState = e.sqlState();
            sqlResultData.sqlError = e.sqlError();
        }
        catch (Opal::Exception &e)
        {
            sqlResultData.isException = true;
            sqlResultData.errorMessage = e.what();
        }

        return sqlResultData;
    }

    void clearCompletedWorkerThreads()
    {
        workers.remove_if([] (auto &thread) {
            if (thread.joinable())
            {
                thread.join();
                return true;
            }
            return false;
        });
    }

protected:
    boost::asio::io_service &io_service;
    std::string ipAddress;
    int port;
    std::string dbName;    
    std::list<std::thread> workers;
};

#endif

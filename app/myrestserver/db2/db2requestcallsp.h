#ifndef DB2_REQUEST_CALL_SP_H
#define DB2_REQUEST_CALL_SP_H

#include "db2request.h"
#include "sql/sqlcall.h"
#include "algo/flatten.h"

class Db2RequestCallSP : public Db2Request
{
public:
    Db2RequestCallSP(Opal::SQL::SqlCall &&call);

    void exec(
        Opal::Database::Db2Connection &conn,
        Opal::SQL::SqlResultData &sqlResultData) const;

protected:
    std::string buildSql(const Opal::SQL::SqlCall &sqlCall) const;

    Opal::SQL::SqlCall call;
};

#endif

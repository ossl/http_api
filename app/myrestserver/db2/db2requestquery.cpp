#include "db2requestquery.h"
#include "algo/flatten.h"

Db2RequestQuery::Db2RequestQuery(Opal::SQL::SqlQuery &&query) :
    Db2Request(), query(std::move(query))
{}

void Db2RequestQuery::exec(
    Opal::Database::Db2Connection &conn,
    Opal::SQL::SqlResultData &sqlResultData) const
{
    auto stat = conn.createStatement();

    execSQLDirect(stat, sqlResultData, buildSql(query));
}

std::string Db2RequestQuery::buildSql( const Opal::SQL::SqlQuery &sqlQuery ) const
{
    std::string seperator;
    std::string sql = "SELECT ";

    if (sqlQuery.select.size())
    {
        sql += Opal::algo::flatten(sqlQuery.select, ", ");
    }
    else
    {
        sql += "*";
    }

    sql += " FROM " + sqlQuery.from;

    if (sqlQuery.where.size() > 0)
    {
        sql += " WHERE ";
        for (auto tok : sqlQuery.where)
        {
            sql += tok.toString() + " ";
        }
    }

    if (sqlQuery.groupBy.size() > 0)
    {
        sql += " GROUP BY ";
        sql += Opal::algo::flatten(sqlQuery.groupBy, ",");
    }

    if (sqlQuery.orderBy.size() > 0)
    {
        sql += " ORDER BY ";
        sql += Opal::algo::flatten(sqlQuery.orderBy, ",");
    }

    if (sqlQuery.offset > 0)
    {
        sql += " OFFSET " + std::to_string(sqlQuery.offset);
    }

    if (sqlQuery.limit > 0)
    {
        sql += " LIMIT " + std::to_string(sqlQuery.limit);
    }

    return sql;
}

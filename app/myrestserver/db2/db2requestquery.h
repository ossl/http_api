#ifndef DB2_REQUEST_QUERY_H
#define DB2_REQUEST_QUERY_H

#include "db2request.h"
#include "sql/sqlquery.h"
#include "algo/flatten.h"

class Db2RequestQuery : public Db2Request
{
public:
    Db2RequestQuery(Opal::SQL::SqlQuery &&query);

    void exec(
        Opal::Database::Db2Connection &conn,
        Opal::SQL::SqlResultData &sqlResultData) const;

protected:
    std::string buildSql(const Opal::SQL::SqlQuery &sqlQuery) const;

    Opal::SQL::SqlQuery query;
};

#endif

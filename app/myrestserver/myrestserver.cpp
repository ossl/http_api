#include "myrestserver.h"

#include "db2/db2client.h"
#include "db2/db2requestquery.h"
#include "db2/db2requestcallsp.h"

#include "http/httpserver.h"

#include "sql/sqlrestinterpretter.h"

#include <memory>

MyRestServer::MyRestServer()
{}

void MyRestServer::start( std::shared_ptr<Opal::LoggerFactory> loggerFactory,
                          boost::asio::io_service &io_service,
                          boost::asio::ip::tcp::endpoint endpoint,
                          boost::asio::ssl::context *ssl_context )
{
    auto httpServer = std::make_shared<Opal::HTTP::Server>(
        loggerFactory, io_service, endpoint, ssl_context);

    auto db_client = std::make_shared<Db2Client>(io_service, "127.0.0.1", 50002, "foo");

    httpServer->async_listen( { Opal::HTTP::method_get },
        [db_client] (
            Opal::HTTP::Request &httpRequest,
            Opal::HTTP::ResponseHandler &&handler)
		{
            Opal::SQL::SqlRestInterpretter interpretter;

            auto dbRequest = std::make_unique<Db2RequestQuery>(
                interpretter.makeQuery(httpRequest));
            
            db_client->async_exec(
                "db2inst1", "nibbles123", std::move(dbRequest),
                [handler{move(handler)}] (Opal::SQL::SqlResultData &&sqlResultData)
			    {
	                Opal::HTTP::Response httpResponse(200);

                    httpResponse.headers["Content-Type"] = "application/json";
				    /*
                    httpResponse.body += Opal::SQL::helpers::to_json_string(
                        std::move(sqlResultData));
                    */
				    httpResponse.body +=
                        Opal::SQL::to_json(std::move(sqlResultData)).toString(true, 8);
	            
                    handler(std::move(httpResponse));
                });
        });

    httpServer->async_listen( { Opal::HTTP::method_post, Opal::HTTP::method_put },
        [db_client] (
            Opal::HTTP::Request &httpRequest,
            Opal::HTTP::ResponseHandler &&handler)
		{
            Opal::SQL::SqlRestInterpretter interpretter;

            auto dbRequest = std::make_unique<Db2RequestCallSP>(
                interpretter.makeCall(httpRequest));

            db_client->async_exec(
                "db2inst1", "nibbles123", std::move(dbRequest),
                [handler{move(handler)}] (Opal::SQL::SqlResultData &&sqlResultData)
			    {
	                Opal::HTTP::Response httpResponse(200);

                    httpResponse.headers["Content-Type"] = "application/json";
				    httpResponse.body +=
                        Opal::SQL::to_json(std::move(sqlResultData)).toString(true, 8);

                    /*
				    httpResponse.body += Opal::SQL::helpers::to_json_string(
                        std::move(sqlResultData));
                        */
	            
                    handler(std::move(httpResponse));
                });
        });

    httpServer->start();
}

#include "mywebserver.h"
#include "http/httpserver.h"

#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <stdio.h>

MyWebServer::MyWebServer() {}

void MyWebServer::start (
    std::shared_ptr<Opal::LoggerFactory> loggerFactory,
    boost::asio::io_service &io_service,
    boost::asio::ip::tcp::endpoint endpoint,
    boost::asio::ssl::context *ssl_context )
{
    auto httpServer = std::make_shared<Opal::HTTP::Server>(
        loggerFactory, io_service, endpoint, ssl_context);

    httpServer->async_listen (
        { Opal::HTTP::method_get },
        [] (const Opal::HTTP::Request &request, Opal::HTTP::ResponseHandler &&handler)
        {
            std::string fileName = "www" + request.uri;
            std::string::size_type idx = fileName.rfind('/');

            if (idx + 1 == fileName.length())
                fileName += "index.html";

            idx = fileName.rfind('.');

            std::string extension;

            if (idx != std::string::npos)
                extension = fileName.substr(idx + 1);
            else
                extension = "text";

            std::ifstream file(fileName.c_str(),
                std::ios::in | std::ios::binary | std::ios::ate);

            if (file.is_open())
            {
                std::size_t length = file.tellg();
                char *memblock = new char[length];
                file.seekg(0, std::ios::beg);
                file.read(memblock, length);
                file.close();

                memblock[length] = 0;

                Opal::HTTP::Response httpResponse(200);
                if (extension == "html")
                    httpResponse.headers["Content-Type"] = "text/html";
                else if (extension == "txt")
                    httpResponse.headers["Content-Type"] = "text";
                else
                    httpResponse.headers["Content-Type"] = "mime/" + extension;

                httpResponse.body.append(memblock, length);

                handler(std::move(httpResponse));

                delete[] memblock;
            }
            else
            {
                Opal::HTTP::Response httpResponse(404);
                httpResponse.headers["Content-Type"] = "text/html";
                httpResponse.body = "File Not found\r\n";

                handler(std::move(httpResponse));
            }
        });

	httpServer->start();
}

#ifndef MY_WEB_SERVER_H
#define MY_WEB_SERVER_H

#include "logger/loggerfactory.h"
#include "logger/logger.h"

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

#include <memory>

class MyWebServer //: public std::enable_shared_from_this<MyRestServer>
{
public:
    MyWebServer();
    
    void start( std::shared_ptr<Opal::LoggerFactory> loggerFactory,
                boost::asio::io_service &io_service,
                boost::asio::ip::tcp::endpoint endpoint,
                boost::asio::ssl::context *ssl_context = nullptr );

};

#endif

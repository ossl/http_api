#ifndef SETTINGS_H
#define SETTINGS_H

#include <string>

// MAS: Temporary solution
// MAS: Hard coded nastyness

class Settings
{
public:
    int dbIpPort = 50002;
    std::string dbIpAddress = "127.0.0.1";
    std::string dbName = "FOO";

    bool httpServerEnabled = true;
    int  httpServerPort = 6100;

    bool httpsServerEnabled = false;
    int  httpsServerPort = 6200;
};

#endif

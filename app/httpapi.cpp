// Local headers
#include "settings.h"
#include "logger/stdloggerfactory.h"
#include "myrestserver/myrestserver.h"
#include "mywebserver/mywebserver.h"

// STL headers
#include <iostream>
#include <fstream>
#include <string>

// System headers
#include <stdio.h>


boost::asio::ssl::context ssl_context(boost::asio::ssl::context::sslv23);

void initSSL()
{
    try
	{
		ssl_context.set_options(
			boost::asio::ssl::context::default_workarounds
			| boost::asio::ssl::context::no_sslv2
			| boost::asio::ssl::context::single_dh_use
			);

		ssl_context.use_private_key_file(
            "/home/mark/localssl/ossl.io/combined",
            boost::asio::ssl::context::pem);

		ssl_context.use_certificate_file(
            "/home/mark/localssl/ossl.io/certificates",
            boost::asio::ssl::context::pem);
		
	}
	catch (std::exception const& e)
	{
        throw e;
	}
}

int main(int argc, char *argv[])
{
    auto loggerFactory = std::make_shared<Opal::StdLoggerFactory>();
    auto logger = loggerFactory->createLogger("SERVICE", "");

    initSSL();

	boost::asio::io_service io_service;

    // Start the REST Server

    boost::asio::ip::tcp::endpoint restServerEndpoint(
        boost::asio::ip::tcp::v4(), 7100);

    auto myRestServer = std::make_shared<MyRestServer>();
    myRestServer->start(loggerFactory, io_service, restServerEndpoint, &ssl_context);

    // Start the Web Server

    boost::asio::ip::tcp::endpoint webServerEndpoint(
        boost::asio::ip::tcp::v4(), 7500);

    auto myWebServer = std::make_shared<MyWebServer>();
    myWebServer->start(loggerFactory, io_service, webServerEndpoint, &ssl_context);

    // Start ASYNC services

    logger->info("Running IO Service..");

	io_service.run();

	logger->info("ASIO has finished.\r\n");

	return 1;
}

# Opal Software Solutions Limited
# Mark Simonetti

OBJDIR = obj

# Addition Packages.
SRC_PACKAGES = BOOST DB2
LIB_PACKAGES = BOOST OPENSSL DB2


# Boost C++ library
ROOT_BOOST = /home/mark/devel/lib/boost
ROOT_OPENSSL = /home/mark/devel/lib/openssl

# DB2
ROOT_DB2 = /home/mark/ibm/dsdriver

#
# Turn on BFD by compiling with
# make -DHASBFD=1
DFLAGS_1 = -DBACKWARD_HAS_BFD # https://github.com/bombela/backward-cpp
DFLAGS   = $(DFLAGS_$(HASBFD))

# DB2


EXTRA_INCLUDE_DIR = $(foreach loop, $(SRC_PACKAGES) $(LIB_PACKAGES), -I$(ROOT_$(loop))/include)
EXTRA_LIB_DIR     = $(foreach loop, $(LIB_PACKAGES),                 -L$(ROOT_$(loop))/lib)

# Compiler
CXX			= g++ 
CC			= $(CXX)

# C Preprocessor Flags
CPPFLAGS	+= -Wall -g -O2 -MMD -MP $(EXTRAFLAGS) $(DFLAGS) -Iinclude
CFLAGS		+=
CXXFLAGS	+= -std=c++20 -fmax-errors=1

# Linker
LDFLAGS		+= $(EXTRA_LIB_DIR)
LDLIBS		+= -lpthread -lssl -lcrypto -ldb2
#LDLIBS		+= -pg -lpthread -lssl -lcrypto

# Lib / Application
EXEC			= httpapi

#SOURCES         = $(shell find src -name '*.cpp') $(shell find app -name '*.cpp')
SOURCES = $(shell find src -name '*.cpp')
#OBJECTS = $(patsubst %.cpp, $(OBJDIR)/%.o, $(SOURCES))
OBJECTS = $(SOURCES:%.cpp=obj/%.o)
DEPENDS = $(OBJECTS:.o=.d)

SOURCES_REST = $(shell find app -name '*.cpp')
OBJECTS_REST = $(SOURCES_REST:%.cpp=obj/%.o)
DEPENDS_REST = $(OBJECTS_REST:.o=.d)

SOURCES_TEST_JSON = $(shell find testing -name '*.cpp')
OBJECTS_TEST_JSON = $(SOURCES_TEST_JSON:%.cpp=obj/%.o)
DEPENDS_TEST_JSON = $(OBJECTS_TEST_JSON:.o=.d)

all: $(EXEC) test_json

$(EXEC): $(OBJECTS) $(OBJECTS_REST)
	$(CXX) ${OBJECTS} $(OBJECTS_REST) -o ${EXEC} ${LDFLAGS} ${LDLIBS}

test_json: $(OBJECTS) $(OBJECTS_TEST_JSON)
	$(CXX) ${OBJECTS} $(OBJECTS_TEST_JSON) -o testing/test_json ${LDFLAGS} ${LDLIBS}

-include $(DEPENDS)
-include $(DEPENDS_REST)
-include $(DEPENDS_TEST_JSON)

$(OBJDIR)/%.o: %.cpp
	mkdir -p $(dir $@)
	$(CXX) ${CPPFLAGS} ${CXXFLAGS} ${EXTRA_INCLUDE_DIR} -c $< -o $@

clean:
	$(RM) $(EXEC) testing/test_json $(OBJECTS) $(OBJECTS_REST) $(OBJECTS_TEST_JSON) $(DEPENDS)



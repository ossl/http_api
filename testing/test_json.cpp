// Local headers
#include "json/json.h"
#include "json/json_builder.h"

// STL headers
#include <iostream>
#include <fstream>
#include <string>

// System headers
#include <stdio.h>

int main(int argc, char *argv[])
{
    Opal::JSON::JSON_Value json;

    json.object["foo1"] = "f1";
    json.object["foo2"] = "f2";
    json.object["foo3"].object["bar1"] = "f3b1";
    json.object["foo3"].object["bar2"] = "f3b2";
    json.object["foo3"].object["bar3"] = { "f3b3v1", "f3b3v2" };
    json.object["foo3"].object["bar4"] = { "f3b4v1", Opal::JSON::Literal::Null };
    json.object["foo3"].object["bar5"] = { json, json };
    json.object["foo3"].object["bar6"] = { };

    std::cout << json.toString(true, 8) << std::endl;

    Opal::JSON::JSON_Builder builder;
    Opal::JSON::JSON_Value json_rebuild = builder.parse(json.toString(true, 8));

    std::cout << json_rebuild.toString(true, 8) << std::endl;

    if (json.toString() == json_rebuild.toString())
    {
        std::cout << "Rebuild pass: match" << std::endl;
    }
    else
    {
        std::cout << "Rebuild fail: mismatch" << std::endl;
    }
   
	return 1;
}

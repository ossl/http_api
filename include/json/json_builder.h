#ifndef OPAL_JSON_BUILDER_H
#define OPAL_JSON_BUILDER_H

#include "json.h"
#include "scanner/scanner.h"
#include <assert.h>

namespace Opal::JSON {

class JSON_Builder
{
public:
    JSON_Builder()
    {

    }

    JSON_Value parse(const std::string &in)
    {
        Scanner scanner(in);
        return value(scanner);
    }

protected:
    JSON_Value value(Scanner &scanner)
    {
        JSON_Value v;

        scanner.skip_white_space();

        if (!scanner.at_end())
        {
            scanner.mark_position();

            char c = scanner.advance();

            switch (c)
            {
                case '{':
                    v.type = Type::Object;
                    v.object = object(scanner);
                    break;
                case '[':
                    v.type = Type::Array;
                    v.array = array(scanner);
                    break;
                case '"':
                    v.type = Type::String;
                    v.value = string(scanner);
                    break;
                default:
                    if (std::isdigit(c))
                    {
                        v.type = Type::Number;
                        v.value = number(scanner);
                    }
                    else if (std::isalpha(c))
                    {
                        while (!scanner.at_end())
                        {
                            char c = scanner.peek();
                            if (!isalpha(c))
                            {
                                v.type = Type::Literal;
                                std::string s = scanner.get_marked_string();
                                if (s == "null") v.literal = Literal::Null;
                                else if (s == "false") v.literal = Literal::False;
                                else if (s == "true") v.literal = Literal::True;
                                break;
                            }
                            else
                            {
                                scanner.advance();
                            }
                        }
                        
                    }
                    else
                    {
                        std::cout << "json: unexpected char '" << c << "' while parsing value" << std::endl;
                    }
                    

            }
        }
        
        return v;
    }

    JSON_Object object(Scanner &scanner)
    {
        JSON_Object obj;
        scanner.skip_white_space();
        bool done = false;
        while (!done && !scanner.at_end())
        {
            scanner.mark_position();

            char c = scanner.advance();

            switch (c)
            {
                case '"':
                    member(obj, scanner);
                    break;
                case '}':
                    done = true;
                    break;
                default:
                    break;
            }

            scanner.skip_white_space();
        }

        return obj;
    }

    void member(JSON_Object &obj, Scanner &scanner)
    {
        std::string memberName = string(scanner);
        scanner.skip_white_space();
        if (!scanner.at_end())
        {
            char c = scanner.advance();
            if (c == ':')
            {
                obj[memberName] = value(scanner);
            }
        }
    }

    JSON_Array array(Scanner &scanner)
    {
        JSON_Array arr;
        
        if (scanner.peek() != ']')
        {
            arr.push_back(value(scanner));
        }

        bool done = false;

        while (!done && !scanner.at_end())
        {
            scanner.skip_white_space();

            char c = scanner.advance();

            switch (c)
            {
                case ']':
                    done = true;
                    break;
                case ',':
                    arr.push_back(value(scanner));
                    break;
                default:
                    std::cout << "json: unexpected char '" << c << "' while parsing array" << std::endl;
                    break;
            }
        }

        return arr;
    }

    std::string string(Scanner &scanner)
    {
        std::string s;
        bool done = false;

        // We don't want to include the opening quote marks that
        // got us here, so mark the new start position here.

        scanner.mark_position();

        while (!done && !scanner.at_end())
        {
            char c = scanner.peek();

            switch (c)
            {
                case '\'':
                    scanner.advance();
                    break;
                case '"':
                    s = scanner.get_marked_string();
                    done = true;
                    break;
                default:
                    break;
            }

            scanner.advance();
        }

        return s;
    }

    std::string number(Scanner &scanner)
    {
        while (std::isdigit(scanner.peek()))
        {
            scanner.advance();
        }
        
        if (scanner.peek() == '.' && std::isdigit(scanner.peek_next()))
        {
            scanner.advance();

            while (std::isdigit(scanner.peek()))
            {
                scanner.advance();
            }
        }

        return scanner.get_marked_string();
    }
};

}

#endif

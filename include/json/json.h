#ifndef OPAL_JSON_H
#define OPAL_JSON_H

// NOTE: Very rough and ready JSON implemention.
// TODO: Check move semantics are being used where needed.
// TODO: Tidy.

#include "scanner/scanner.h"
//#include "string/sub_string.h"

#include <string>
#include <vector>
#include <map>
#include <iostream>

namespace Opal::JSON {

enum class Type {
    Literal, Number, String, Object, Array
};

enum class Literal {
    False, True, Null
};

struct JSON_Value;
typedef std::map<std::string, JSON_Value> JSON_Object;
typedef std::vector<JSON_Value> JSON_Array;

std::string escape(const std::string &in);

struct JSON_Value
{
    JSON_Value() :
        type(Type::Object) {}
    
    JSON_Value(Type t) :
        type(t) {}
    
    JSON_Value(Literal literal) :
        type(Type::Literal), literal(literal) {}

    JSON_Value(const std::string &s) :
        type(Type::String), value(escape(s)) {}
      
    JSON_Value(const char *s) :
        type(Type::String), value(escape(s)) {}

    JSON_Value(const JSON_Value &other) = default;
    JSON_Value(JSON_Value &&other) = default;

    JSON_Value(JSON_Object &&object) :
        type(Type::Object), object(std::move(object)) {}
    
    JSON_Value(const JSON_Object &object) :
        type(Type::Object), object(object) {}

    JSON_Value(JSON_Array &&array) :
        type(Type::Array), array(std::move(array)) {}

    JSON_Value(const JSON_Array &array) :
        type(Type::Array), array(array) {}

    template <class T>
    JSON_Value(std::vector<std::vector<T> > init_list) :
        type(Type::Array)
    {
        for (const auto &arr : init_list)
        {
            this->array.push_back(arr);
        }
    }

    template <class T>
    JSON_Value(const std::vector<T> &array_in) :
        type(Type::Array)
    {
        for (const T &elem : array_in)
        {
            array.push_back(elem);
        }
    }

    JSON_Value(std::initializer_list<JSON_Value> init_list) :
        type(Type::Array), array(init_list)
    {}

    JSON_Value& operator=(const JSON_Value &other) = default;
    JSON_Value& operator=(JSON_Value &&other) = default;

    std::string toString(bool pretty = false, int indent_size = 4, int indent = 0) const
    {
        std::string s;
        switch (type)
        {
            case Type::Literal:
                switch (literal)
                {
                    case Literal::False: s += "false"; break;
                    case Literal::True: s += "true"; break;
                    case Literal::Null: s += "null"; break;
                    default: s += "invalid";
                }
                break;
            case Type::Number:
                s += value; break;
            case Type::String:
                s += "\"" + value + "\""; break;
            case Type::Object:
            {
                s += "{";
                std::string sep;
                if (pretty) indent += indent_size;
                for (auto const &member : object)
                {
                    s += sep;
                    if (pretty)
                    {
                        s += "\r\n";
                        s += std::string(indent, ' ');
                    }
                    s += "\"" + member.first + "\": ";
                    s += member.second.toString(pretty, indent_size, indent);
                    sep = ",";
                }
                if (pretty)
                {
                    indent -= indent_size;
                    s += std::string(indent, ' ');
                }
                if (pretty)
                {
                    s += "\r\n";
                    s += std::string(indent, ' ');
                }
                s += "}";
                break;
            }
            case Type::Array:
            {
                s += "[";
                std::string sep;
                for (auto const &item : array)
                {
                    s += sep;
                    s += item.toString(pretty, indent_size, indent);
                    sep = ",";
                }
                s += "]";
                break;
            }
        }

        return s;
    }

    Type type;
    Literal literal;
    std::string value;
    JSON_Object object;
    JSON_Array array;
};

}

#endif

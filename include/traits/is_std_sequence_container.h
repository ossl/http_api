#ifndef OPAL_IS_STD_SEQUENCE_CONTAINER_H
#define OPAL_IS_STD_SEQUENCE_CONTAINER_H

#pragma once

#include <vector>
#include <list>
#include <forward_list>
#include <array>
#include <deque>

namespace Opal {

template <class T>
struct is_std_sequence_container
{
    static bool const value = false;
};

template <class T>
struct is_std_sequence_container<std::vector<T>>
{
    static bool const value = true;
};

template <class T>
struct is_std_sequence_container<std::list<T>>
{
    static bool const value = true;
};

template <class T>
struct is_std_sequence_container<std::forward_list<T>>
{
    static bool const value = true;
};

template <class T, size_t n>
struct is_std_sequence_container<std::array<T, n>>
{
    static bool const value = true;
};

template <class T>
struct is_std_sequence_container<std::deque<T>>
{
    static bool const value = true;
};

} // Opal

#endif

#ifndef OPAL_IS_STD_STRING_H
#define OPAL_IS_STD_STRING_H

#pragma once

#include <string>

namespace Opal {

template <class T>
struct is_std_string
{
    static bool const value = false;
};

template <>
struct is_std_string<std::string>
{
    static bool const value = true;
};

} // Opal

#endif

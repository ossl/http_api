#ifndef OPAL_SOCKET_ASIO_H
#define OPAL_SOCKET_ASIO_H

#pragma once

// Boost headers
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

// STL headers
#include <functional>
#include <memory>
#include <iostream>

namespace Opal {

class Socket
{
public:
	Socket(boost::asio::io_service &io_service);
    Socket(Socket&&);

    virtual ~Socket();

    //const boost::asio::io_service & io_service();

    bool isSSL() const;
    void cancel();
    void close();

    void enableSSL(boost::asio::ssl::context &ssl_context);

	boost::asio::ip::tcp::socket & tcp_socket();

    template <class Handler>
    void handshakeSSL(Handler&& handler)
    {
        if (!isSSL())
        {
            throw std::exception();
        }

        ssl_stream->async_handshake(
    		boost::asio::ssl::stream_base::server, std::move(handler));
    }

    template <class Handler>
    void shutdownSSL(Handler&& handler)
    {
        if (!isSSL())
        {
            throw std::exception();
        }

        ssl_stream->async_shutdown(std::move(handler));
    }

    template <class Allocator, class CompletionCondition, class ReadHandler>
    void async_read(
        boost::asio::basic_streambuf<Allocator>& b,
        CompletionCondition completion_condition,
        ReadHandler&& handler )
    {
        if (isSSL())
        {
            boost::asio::async_read(
                *ssl_stream, b, completion_condition, std::move(handler));
        }
        else
        {
            boost::asio::async_read(
                socket, b, completion_condition, std::move(handler));
        }
    }

    template <class Allocator, class MatchCondition, class ReadHandler>
    void async_read_until(
        boost::asio::basic_streambuf<Allocator> & b,
        MatchCondition match_condition,
        ReadHandler && handler )
    {
        if (isSSL())
        {
            boost::asio::async_read_until(
                *ssl_stream, b, match_condition, std::move(handler));
        }
        else
        {
            boost::asio::async_read_until(
                socket, b, match_condition, std::move(handler));
        }
    }

    // BOOST ASIO wrappers: async_write

    template <class ConstBufferSequence, class WriteHandler>
    void async_write(
        const ConstBufferSequence & buffers,
        WriteHandler && handler )
    {
        if (isSSL())
        {
            boost::asio::async_write(
                *ssl_stream, buffers, handler);
        }
        else
        {
            boost::asio::async_write(
                socket, buffers, handler);
        }
    }

protected:
    boost::asio::io_service &io_service_ref;
    boost::asio::ip::tcp::socket socket;

    std::unique_ptr<
        boost::asio::ssl::stream<boost::asio::ip::tcp::socket&> > ssl_stream;

    bool is_shutdown_called;
};

} // Opal

#endif

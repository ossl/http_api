#ifndef OPAL_SERVER_SESSION_FACTORY_H
#define OPAL_SERVER_SESSION_FACTORY_H

#pragma once

#include <boost/asio.hpp>

namespace Opal {

class LoggerFactory;

class Session;
//class Server;
class Socket;

class SessionFactory
{
public:
	virtual ~SessionFactory() {}

    virtual std::shared_ptr<Session>
    /*
    createSession( boost::asio::io_service &io_service,
                   std::shared_ptr<LoggerFactory> loggerFactory ) = 0;
                   */
    createSession( Socket &&socket,
                   std::shared_ptr<LoggerFactory> loggerFactory ) = 0;

};

} // Opal

#endif

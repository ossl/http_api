#ifndef OPAL_SERVER_H
#define OPAL_SERVER_H

#pragma once

// Local headers
#include "logger/loggerfactory.h"
#include "logger/logger.h"
#include "sessionfactory.h"
#include "session.h"
#include "socket.h"

// Boost headers
#include <boost/asio.hpp>

// STL headers
#include <iostream>
#include <string>
#include <memory>
#include <forward_list>

namespace Opal {

class Server : public std::enable_shared_from_this<Server>
{
public:
	Server( std::shared_ptr<LoggerFactory> loggerFactory,
            std::shared_ptr<SessionFactory> sessionFactory,
            boost::asio::io_service &io_service,
		    const boost::asio::ip::tcp::endpoint &endpoint,
            boost::asio::ssl::context *ssl_context = nullptr );

	void start();
	void stop();

protected:
    void startHouseKeepingTimer();
	void startAccept();

	boost::asio::io_service &io_service;
	boost::asio::ip::tcp::acceptor acceptor;
    boost::asio::deadline_timer acceptorRestartTimer;
    boost::asio::deadline_timer houseKeepingTimer;

    Socket socket;

	std::shared_ptr<SessionFactory> sessionFactory;
    std::shared_ptr<LoggerFactory> loggerFactory;
    std::unique_ptr<Logger> logger;

    boost::asio::ssl::context *ssl_context;

    // Keep a weak references to created sessions so we
    // can cancel them on server shutdown.

	std::forward_list<std::weak_ptr<Session>> sessions;
};

} // Opal

#endif

#ifndef OPAL_SERVER_SESSION_H
#define OPAL_SERVER_SESSION_H

#pragma once

#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

namespace Opal {

class Socket;

class Session
{
public:
	virtual ~Session() {}
	virtual void start() = 0;
	virtual void startSSL(boost::asio::ssl::context &ssl_context) = 0;
    virtual void cancel() = 0;
    //virtual Socket& socket() = 0;

};

} // Opal

#endif

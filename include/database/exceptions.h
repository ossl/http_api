#ifndef OPAL_DATABASE_EXCEPTIONS_H
#define OPAL_DATABASE_EXCEPTIONS_H

#pragma once

#include "exception.h"
#include "detail/diagnostic_data.h"

#include <vector>
#include <string>

namespace Opal::Database {

class DriverException : public std::exception
{
public:
    DriverException(std::vector<detail::DiagnosticData> &&diagnosticData) :
        std::exception(), diagnosticRecords(std::move(diagnosticData))
    {}

    const char * what() const noexcept
    {
        return "Database Driver Exception";
    }

    std::string sqlState() const noexcept
    {
        if (diagnosticRecords.size() == 0) return "000000";
        return diagnosticRecords.at(0).sqlState;
    }

    std::string sqlError() const noexcept
    {
        if (diagnosticRecords.size() == 0) return "None";
        return diagnosticRecords.at(0).message;
    }

    std::vector<detail::DiagnosticData> diagnosticRecords;
};

struct AllocEnvironmentException : public Opal::Exception
{
    AllocEnvironmentException() :
        Exception("Could Not Allocate SQL Environment")
    {}
};

struct NoSuchColumnNameException : public Opal::Exception
{
    NoSuchColumnNameException(std::string columnName) :
        Exception("No Such Column Name Exception: '" + columnName + "'"),
        columnName(std::move(columnName))
    {}

    std::string columnName;
};

template <class T>
struct NoSuchColumnIndexException : public Opal::Exception
{
    NoSuchColumnIndexException(T index) :
        Exception("No Such Column Index Exception: " + std::to_string(index)),
        index(index)
    {}
    
    T index;
};

struct NoDataException : public Opal::Exception
{
    NoDataException() :
        Exception("No Data Exception")
    {}
};

} // Opal::Database

#endif

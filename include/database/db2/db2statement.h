///
//  Lightweight IBM DB2 Driver wrapper
//  Opal Software Solutions Limited (C) 2020
//  Mark Simonetti
//  22.06.2020
//

#ifndef OPAL_DATABASE_DB2_STATEMENT_H
#define OPAL_DATABASE_DB2_STATEMENT_H

#pragma once

#include "db2resultset.h"

#include "detail/get_diagnostic_records.h"

// Db2
#include <sqlcli1.h>

// STL
#include <string>
#include <memory>

namespace Opal::Database {

class Db2Statement
{
public:
    Db2Statement(SQLHANDLE hndConn);

    virtual ~Db2Statement();

    void exec(const std::string &sql);

    Db2ResultSet query(const std::string &sql);

    std::vector<detail::DiagnosticData> diagnosticData();
    bool diagnosticData(detail::DiagnosticData &diagnosticData, int index = 1);

public:
    std::string lastQuery;

    SQLHANDLE hndStat = SQL_INVALID_HANDLE;
};

} // Opal::Database

#endif

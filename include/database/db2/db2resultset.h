///
//  Lightweight IBM DB2 Driver wrapper
//  Opal Software Solutions Limited (C) 2020
//  Mark Simonetti
//  22.06.2020
//

#ifndef OPAL_DATABASE_DB2_RESULTSET_H
#define OPAL_DATABASE_DB2_RESULTSET_H

#pragma once

#include "database/db2/db2resultset.h"
#include "database/detail/diagnostic_data.h"

// Db2
#include <sqlcli1.h>

#include <string>
#include <vector>
#include <map>
#include <set>
#include <iostream>

namespace Opal::Database {

class Db2ResultSet
{  
    struct ColInfo
    {
        SQLCHAR columnName[128];
        SQLSMALLINT columnNameLength;
        SQLSMALLINT columnType;
        SQLUINTEGER columnSize;
        SQLSMALLINT columnScale;
        SQLSMALLINT isNullable;
        SQLSMALLINT columnDisplaySize;
    };

    struct RowInfo
    {
        SQLINTEGER bufferLength;
        SQLINTEGER returnLength;
        SQLCHAR * data;
    };

public:
    Db2ResultSet(SQLHANDLE hndStat);

    bool next(bool dumpToOutput = false);

    std::size_t numCols();

    std::string data(std::size_t columnIdx, bool trim = true);
    std::string data(const std::string &columnName, bool trim = true);
    std::string columnName(std::size_t columnIdx);
    //std::set<std::string> columnNames();
    std::map<std::string, std::size_t> columnNameMap();
    std::vector<std::string> columnNames();
    std::vector<detail::DiagnosticData> diagnosticData();
    bool diagnosticData(detail::DiagnosticData &diagnosticData, int index = 1);


private:
    bool fetch(bool dumpToOutput = false);
    void loadColInfo();
    void loadRowInfo();

    ColInfo *colInfo = NULL;
    RowInfo *rowInfo = NULL;
    std::map<std::string, std::size_t> fieldMap;
    std::size_t fetchCount = 0;
    std::size_t rowCount = 0;

public:
    SQLHANDLE hndStat = SQL_INVALID_HANDLE;
};

} // Opal::Database

#endif
 
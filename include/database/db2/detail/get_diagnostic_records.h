#ifndef OPAL_DATABASE_DB2_GET_DIAGNOSTIC_RECORDS_H
#define OPAL_DATABASE_DB2_GET_DIAGNOSTIC_RECORDS_H

#pragma once

#include "database/detail/diagnostic_data.h"

#include <sqlcli1.h>

#include <string>
#include <vector>

namespace Opal::Database::DB2::detail
{
    SQLRETURN get_diagnostic_record(
        Opal::Database::detail::DiagnosticData &diagnosticData,
        SQLSMALLINT index,
        SQLHANDLE   sqlHandle,
        SQLSMALLINT sqlHandleType);

    std::vector<Opal::Database::detail::DiagnosticData> get_diagnostic_records(
        SQLHANDLE   sqlHandle,
        SQLSMALLINT sqlHandleType);
        
} // Opal::Database::detail

#endif

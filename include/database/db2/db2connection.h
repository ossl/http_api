///
//  Very lightweight Db2 wrapper
//  Opal Software Solutions Limited (C) 2020
//  Mark Simonetti
//  22.06.2020
//

#ifndef OPAL_DATABASE_DB2_CONNECTION_H
#define OPAL_DATABASE_DB2_CONNECTION_H

#pragma once

#include "db2statement.h"

// Db2
#include <sqlcli1.h>

// STL
#include <string>
#include <memory>

namespace Opal::Database {

class Db2Connection
{
public:
    enum ASyncSupportType {
        None,
        Connection,
        Statement
    };

    Db2Connection(
        const std::string &server,
        int port,
        const std::string &username,
        const std::string &password,
        const std::string &db
        );

    virtual ~Db2Connection();

    Db2Statement createStatement();

    std::vector<detail::DiagnosticData> diagnosticData();
    bool diagnosticData(detail::DiagnosticData &diagnosticData, int index = 1);

public:
    SQLHANDLE hndEnv  = SQL_INVALID_HANDLE;
    SQLHANDLE hndConn = SQL_INVALID_HANDLE;
    bool isInitSuccess = false;
};

} // Opal::Database

#endif

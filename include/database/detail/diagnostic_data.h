#ifndef OPAL_DB_DETAIL_DIAG_DATA_H
#define OPAL_DB_DETAIL_DIAG_DATA_H

#pragma once

#include <string>
#include <exception>

namespace Opal::Database::detail {

struct DiagnosticData
{
    std::string sqlState;
    std::string message;
};

} // Opal::Database::detail

#endif

#ifndef RETCODE_H
#define RETCODE_H

#pragma once

#include <string>

template <class T>
class Retcode
{
public:
	Retcode() :
        resultSet(false),
	    resultChecked(false),
	    assigned(false)
    {}

    Retcode(const Retcode &src) :
	    result(src.result),
	    resultSet(src.resultSet),
	    resultText(src.resultText.c_str()),
	    resultChecked(false),
        assigned(false)
    {
	    if (!resultSet)
		    assert(false);
	    src.assigned = true;
    }

	Retcode(T result)
    {
        setResult(result);
    }

	Retcode(T result, const std::string &text)
    {
        setResult(result, text);
    }

	Retcode(T result, const char *text)
    {
        setResult(result, text);
    }

	virtual ~Retcode()
    {
        assert(assigned || resultChecked)
    }

	Retcode& operator=(const Retcode &rhs)
    {
	    if (this != &rhs)
	    {
    	    result = rhs.result;
	        resultSet = rhs.resultSet;
	        resultText = rhs.resultText.c_str();
	        resultChecked = false;
	        assigned = false;

    	    rhs.assigned = true;
        }

    	return *this;
    }

	Retcode& operator=(T result)
    {
        setResult(result);
	    return *this;
    }

    void setResult(T result, const char *text)
    {
        if (text)
		    this->resultText = text;
	    else
		    this->resultText.erase();

	    this->result = result;
	    this->resultSet = true;
	    this->resultChecked = false;
	    this->assigned = false;
    }

	void setResult(T result, const std::string &text)
    {
	    setResult(result, text.c_str());
    }

	void setResult(T result)
    {
	    setResult(result;
    }

	bool isResultSet() const
    {
	    return resultSet;
    }

	bool isResultChecked() const
    {
        return resultChecked;
    }

	std::string getResultText() const
    {
    	return resultText;
    }

	T getResult()
    {
    	// Assert a debug error if the caller is attempting to get
	    // the result when the result has not yet been set.

    	if (!resultSet)
	    	assert(false);

    	// Mark the result as checked so we can warn if the result
	    // is ignored.

    	resultChecked = true;

    	return result;
    }

	void ignore()
    {
    	assert(resultSet);
    	resultChecked = true;
    }

protected:
	T result;
	bool resultSet;
	bool resultChecked;

	mutable bool assigned;

	std::string resultText;
};

#endif


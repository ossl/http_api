#ifndef OPAL_ALGO_FLATTEN_H
#define OPAL_ALGO_FLATTEN_H

#pragma once

#include "detail/flatten_impl.h"

namespace Opal {
namespace algo {

template <class T>
std::string flatten(
    const T &item, const std::string &separator )
{
    return detail::flatten(item, separator);
}

} // algo
} // Opal

#endif

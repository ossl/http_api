#ifndef OPAL_ALGO_FLATTEN_IMPL_H
#define OPAL_ALGO_FLATTEN_IMPL_H

#pragma once

#include "traits/is_std_sequence_container.h"
#include "traits/is_std_string.h"
#include <string>
#include <vector>
#include <sstream>

namespace Opal::algo::detail {

std::string flatten(const std::string &item, const std::string &separator);
std::string flatten(int i, const std::string &separator);
std::string flatten(std::size_t s, const std::string &separator);
std::string flatten(float f, const std::string &separator);

template <class T1, class T2>
std::string flatten(
    const std::pair<T1, T2> &p, const std::string &separator )
{
    return flatten(p.first, separator) + separator + flatten(p.second, separator);
}

template <class T>
std::string flatten(
    const T &item, const std::string &separator )
{
    bool is_first = true;
    std::string str;    

    if (Opal::is_std_sequence_container<T>::value)
    {
        for (auto const &subItem : item)
        {
            if (!is_first) str += separator;
            str += flatten(subItem, separator);
            is_first = false;
        }
    }
    else
    {
        str = flatten(item, separator);
    }

    return str;
}

} // Opal::algo::detail

#endif

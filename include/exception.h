#ifndef OPAL_EXCEPTION_H
#define OPAL_EXCEPTION_H

#include <string>
#include <exception>

namespace Opal {

struct Exception : public std::exception
{
    Exception(std::string message) :
        message(std::move(message))
    {}

    const char * what() const noexcept
    {
        return message.c_str();
    }

    std::string message;
};

}

#endif

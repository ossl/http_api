#ifndef OPAL_HTTP_URI_H
#define OPAL_HTTP_URI_H

#pragma once

#include "string/for_each.h"
#include "urldecoder.h"

#include <map>
#include <vector>
#include <string>
#include <functional>

namespace Opal::HTTP {

struct URI
{
public:
    URI(const std::string &uri)
    {
        // NOTE: At the moment we just cut the string up by the
        //       delimiters. No checking for valid characters
        //       or structure is performed.
        //
        // TODO: Implement scanning based on the RFC.
        //
        std::size_t pos = String::for_each(uri, "?#/", 0,
            [&] (std::string sub, char found_delimiter) -> bool {

                if (sub.length() > 0) {
                    path.push_back(URLDecoder::decode(sub));                
                }
                
                return found_delimiter == '/';

            });

        if (pos < uri.size() && uri.at(pos) == '?') {
            std::string key;

            pos = String::for_each(uri, "&#=", pos + 1,
                [&] (std::string sub, char found_delimiter) -> bool {

                    switch (found_delimiter) {
                        case '=':
                            key = sub;
                            return true;
                        case '&':
                        case 0:
                            addQueryComponent(std::move(key), std::move(sub));
                            return true;
                    }

                    return false;

                });
        }
 
        if (pos < uri.size() && uri.at(pos) == '#') {
            fragment = uri.substr(pos + 1);
        }
    }

    std::vector<std::string> path;
    std::map<std::string, std::string> query;
    std::string fragment;

private:
    void addQueryComponent(std::string && key, std::string && val)
    {
        query[URLDecoder::decode(key)] = URLDecoder::decode(val);
    }
};

}

#endif

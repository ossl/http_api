#ifndef OPAL_HTTP_REQUEST_H
#define OPAL_HTTP_REQUEST_H

#pragma once

#include <string>
#include <map>

namespace Opal::HTTP {

struct Request final
{
    Request() {}

    Request( std::string httpVersion,
             std::string sourceIpAddress,
             int sourcePort,
             std::string method,
	         std::string uri,
	         std::string body,
	         std::map<std::string, std::string> &&headers ) :
        httpVersion( std::move(httpVersion) ),
        sourceIpAddress( std::move(sourceIpAddress) ),
        sourcePort( sourcePort ),
        method( std::move(method) ),
        uri( std::move(uri) ),
        body( std::move(body) ),
        headers( std::move(headers) )
    {}

    std::string httpVersion;
    std::string sourceIpAddress;
    int sourcePort;
    std::string method;
    std::string uri;
    std::string body;
	std::map<std::string, std::string> headers;
};

}

#endif

#ifndef OPAL_HTTP_H
#define OPAL_HTTP_H

#pragma once

#include "httpresponse.h"

#include <string>
#include <functional>

namespace Opal::HTTP
{
    typedef std::function<void(Response&&)> ResponseHandler;

    const std::string method_get     = "GET";
    const std::string method_post    = "POST";
    const std::string method_put     = "PUT";
    const std::string method_delete  = "DELETE";
    const std::string method_options = "OPTIONS";

	enum error
	{
		// Success

		OK					= 200,
		Created				= 201,
		Accepted			= 202,

		// Redirection

		NotModified			= 304,

		// Client Error

		BadRequest			= 400,
		Unauthorized		= 401,
		Forbidden			= 403,
		NotFound			= 404,
		Conflict			= 409,
		Locked				= 423,

		// Server Error

		InternalServerError	= 500,
		NotImplemented		= 501,
		ServiceUnavailable	= 503,
		InsufficientStorage	= 507
	};
}

#endif

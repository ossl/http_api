#ifndef OPAL_HTTP_RESPONSE_H
#define OPAL_HTTP_RESPONSE_H

#pragma once

#include <string>
#include <map>

namespace Opal::HTTP {

struct Response final
{
    Response( int httpResponseCode ) :
        httpResponseCode(httpResponseCode)
    {}

    Response( int httpResponseCode,
	          std::string body,
	          std::map<std::string, std::string> headers ) :

        body( std::move(body) ),
        headers( std::move(headers) ),
        httpResponseCode( httpResponseCode )
    {}

    std::string body;
	std::map<std::string, std::string> headers;
    int httpResponseCode;
};

}

#endif

#ifndef OPAL_HTTP_SESSION_H
#define OPAL_HTTP_SESSION_H

#pragma once

// Local headers
#include "httperror.h"
#include "httprequest.h"
#include "httpresponse.h"
#include "httprequestlistener.h"

//#include "server/server.h"
#include "server/session.h"
#include "server/socket.h"
//#include "server/socket.h"

#include "logger/logger.h"
#include "logger/loggerfactory.h"

// Boost headers
//#include <boost/asio/ssl.hpp>

// STL headers
#include <memory>
#include <queue>

namespace Opal::HTTP {

static int DefaultTimeoutSeconds = 5;

class Session :
    public Opal::Session,
    public std::enable_shared_from_this<Session>
{ 
public:
	Session ( Socket &&socket,
              std::shared_ptr<RequestListener> httpRequestListener,
              std::shared_ptr<LoggerFactory> loggerFactory );

    virtual ~Session();

	bool isOpen();
	void cancel();
	void start();
	void startSSL(boost::asio::ssl::context &ssl_context);

protected:
    bool init();
    void interpretRequestHeaders();
    void close_with_warning(const std::string &log_message);
    void async_read_request_line();
    void async_read_header_line();
    void dump_headers();
	void async_read_body();
	void flushReadBufferToBody(std::size_t s);
	void queueCurrentRequest();
    void beginProcessRequestQueue();
	void sendResponse(Request &&httpRequest, Response &&httpResponse);
    void writeBufferAppendLine();
    void writeBufferAppendLine(std::string &&line);
	void writeBufferAppend(std::string &&message);
    void writeBufferFlush();
    void logError(const char *opName, const boost::system::error_code &ec);
	void resetSocketTimeoutTimer(int seconds = DefaultTimeoutSeconds);
	void handleASIOError(const char *opName, const boost::system::error_code &ec);

    void close();

    struct {
    	std::string method;
	    std::string uri;
	    std::string body;
	    std::string version;
    	std::map<std::string, std::string> headers;
    	int contentLength;
    } incoming_request;

    struct {
        int port;
        std::string ip_address;
    } peer;

	boost::asio::deadline_timer socketTimeoutTimer;
	boost::asio::streambuf readBuffer;
    std::string writeBuffer;

    Socket socket;

    std::queue<Request> httpRequestQueue;
    std::shared_ptr<RequestListener> httpRequestListener;
    std::unique_ptr<Logger> logger;

    int idleTimeout;
	bool isStarted;
    bool isPersistant;
};

}

#endif

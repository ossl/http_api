#ifndef OPAL_HTTP_SESSION_FACTORY_H
#define OPAL_HTTP_SESSION_FACTORY_H

#pragma once

// Local headers
#include "httpsession.h"
#include "httprequestlistener.h"
#include "server/sessionfactory.h"
#include "server/session.h"

// STL headers
#include <memory>

namespace Opal {

class LoggerFactory;
class Socket;

}

namespace Opal::HTTP {

class SessionFactory : public Opal::SessionFactory
{
public:
    SessionFactory(std::shared_ptr<RequestListener> httpRequestListener) :
        httpRequestListener(httpRequestListener)
    {}

    std::shared_ptr<Opal::Session> createSession(
        Socket &&socket, std::shared_ptr<LoggerFactory> loggerFactory)
    {
        return std::shared_ptr<Opal::Session>(
            new Session(std::move(socket), httpRequestListener, loggerFactory));
    }

    std::shared_ptr<RequestListener> httpRequestListener;
};

}

#endif

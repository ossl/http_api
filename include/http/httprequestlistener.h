#ifndef OPAL_HTTP_REQUEST_LISTENER_H
#define OPAL_HTTP_REQUEST_LISTENER_H

#pragma once

#include "http.h"
#include "httprequest.h"
#include "httpresponse.h"
#include <functional>

namespace Opal::HTTP {

class RequestListener
{
public:
    RequestListener();

    // Default request handler.

    virtual void onRequest(
        Request &httpRequest, ResponseHandler &&responseHandler);

    // Override this if the handling of the request can be cancelled.

    virtual void cancel();

    void async_listen(
        const std::vector<std::string> &methods,
        std::function<void(Request&, ResponseHandler&&)> &&handler);

protected:
    virtual void sendOptions(ResponseHandler &&handler);

    std::map<std::string, std::function<void(Request&, ResponseHandler&&)>> methodFunctionMap;
};

}

#endif

#ifndef OPAL_HTTP_SERVER_H
#define OPAL_HTTP_SERVER_H

#pragma once

#include "server/server.h"
#include "httprequestlistener.h"

namespace Opal::HTTP {

class Server
{
public:
	Server( std::shared_ptr<LoggerFactory> loggerFactory,
            boost::asio::io_service &io_service,
		    const boost::asio::ip::tcp::endpoint &endpoint,
            boost::asio::ssl::context *ssl_context = nullptr );

    void start();
    void cancel();

    void async_listen(
        const std::vector<std::string> &methods,
        std::function<void(Request&, ResponseHandler&&)> &&handler);

protected:
    std::shared_ptr<Opal::Server> server;
    std::shared_ptr<RequestListener> httpRequestListener;
};

}

#endif

#ifndef OPAL_HTTP_URI_DECODER_H
#define OPAL_HTTP_URI_DECODER_H

#pragma once

#include <string>

namespace Opal::HTTP {

class URLDecoder
{
public:
	static std::string decode(const std::string &sourceURL)
	{
        int len = sourceURL.length();
        if (len == 0)
            return sourceURL;

        std::string dst;

        // To be as efficient as possible with std::string, we need to first reserve
        // the maximum amount of space we will need.

        dst.reserve(len);

        // From C++11 onwards, the underlying structure of std::string is guaranteed
        // to be a contiguous char array, so it is safe to iterate through it in that
        // manner.

        const char *src = sourceURL.c_str();

        char a, b;
		while (*src) {
            // Decode characters encoded into hex (%xx)

			if ((*src == '%') && ((a = src[1]) && (b = src[2])) && (isxdigit(a) && isxdigit(b))) {

                if (a >= 'a') a -= 'a'-'A';
                if (a >= 'A')
					a -= ('A' - 10);
				else
					a -= '0';

                if (b >= 'a') b -= 'a'-'A';
                if (b >= 'A')
					b -= ('A' - 10);
				else
					b -= '0';

                dst.push_back(16 * a + b);

                src += 3;
			}
            else if (*src == '+')
            {
                dst.push_back(' ');
				++src;
			}
            else
            {
                dst.push_back(*src++);
			}
		}

        return dst;
	}
};

}

#endif

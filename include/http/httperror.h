#ifndef OPAL_HTTP_ERROR_H
#define OPAL_HTTP_ERROR_H

#pragma once

namespace Opal::HTTP {
    
class Error
{
public:
	Error() {}
	static const char * toString(int httpErrorCode);
};

}

#endif

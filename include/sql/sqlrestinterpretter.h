#ifndef OPAL_SQL_REST_INTERPRETTER_H
#define OPAL_SQL_REST_INTERPRETTER_H

#pragma once

#include "sql/sqlquery.h"
#include "sql/sqlcall.h"
#include "http/httprequest.h"

namespace Opal::SQL {

class SqlRestInterpretter
{
public:
    SqlRestInterpretter();
 
    SqlQuery makeQuery(const HTTP::Request &httpRequest);
    SqlCall makeCall(const HTTP::Request &httpRequest);
};

} // Opal::SQL

#endif

#ifndef OPAL_SQL_H
#define OPAL_SQL_H

#pragma once

#include <string>

namespace Opal {
namespace SQL {

enum class Order
{
    Ascending,
    Descending
};

struct Token
{
    enum class Type
    {
        // Unary
        Not,
        // Logical operators
        And, Or,
        // Operators
        Add, Subtract, Divide, Multiply, Modulus,
        Equal, NotEqual, More, MoreOrEqual, Less, LessOrEqual, Like,
        // Grouping
        OpenBrace, CloseBrace, OpenParen, CloseParen,
        // Literals
        Ident, String, Number, Null, True, False,
    };

    Token(Type t) :
        type(t)
    {}

    Token(Type t, std::string s) :
        type(t), str(std::move(s))
    {}

    static std::string toString(Token::Type type)
    {
        switch (type)
        {
            case Type::Not: return "NOT";
            case Type::And: return "AND";
            case Type::Or: return "OR";
            case Type::Add: return "+";
            case Type::Subtract: return "-";
            case Type::Divide: return "/";
            case Type::Multiply: return "*";
            case Type::Modulus: return "MOD";
            case Type::Equal: return "=";
            case Type::NotEqual: return "!=";
            case Type::More: return ">";
            case Type::MoreOrEqual: return ">=";
            case Type::Less: return "<";
            case Type::LessOrEqual: return "<=";
            case Type::Like: return "LIKE";
            case Type::OpenBrace: return "{";
            case Type::CloseBrace: return "}";
            case Type::OpenParen: return "(";
            case Type::CloseParen: return ")";
            case Type::Ident: return "IDENT";
            case Type::String: return "STRING";
            case Type::Number: return "NUMBER";
            case Type::Null: return "NULL";
            case Type::True: return "TRUE";
            case Type::False: return "FALSE";
        }

        return "INVALID";
    }

    std::string toString()
    {
        switch (type)
        {
            case Type::String:
            case Type::Number:
            case Type::Ident:
                return str;
            default:
                return toString(type);
        }
    }

    Type type;

    std::string str;
};

} // SQL
} // Opal

#endif

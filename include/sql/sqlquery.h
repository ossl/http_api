#ifndef OPAL_SQL_QUERY_H
#define OPAL_SQL_QUERY_H

#pragma once

#include "sql.h"
#include <string>
#include <list>

namespace Opal::SQL {

struct SqlQuery
{
    std::string from;
    std::list<std::string> select;
    std::list<SQL::Token> where;
    //std::list<std::pair<std::string, SQL::Order>> orderBy;
    std::list<std::string> orderBy;
    std::list<std::string> groupBy;
    int offset = 0;
    int limit  = 0;
};

} // Opal::SQL

#endif

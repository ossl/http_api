#ifndef OPAL_SQL_CALL_H
#define OPAL_SQL_CALL_H

#pragma once

#include "sql.h"
#include <string>
#include <list>

namespace Opal::SQL {

struct SqlCall
{
    std::string stored_proc;
    std::list<std::string> params;
};

} // Opal::SQL

#endif

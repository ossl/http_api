#ifndef OPAL_SQL_RESULT_DATA_H
#define OPAL_SQL_RESULT_DATA_H

#pragma once

#include "database/exceptions.h"

#include "json/json.h"

#include <vector>
#include <string>

namespace Opal {
namespace SQL {

struct SqlResultData
{
    bool isException = false;
    std::string sqlState;
    std::string sqlError;
    std::string errorMessage;
    std::vector<std::string> columns;
    std::vector<std::vector<std::string>> rows;
};

JSON::JSON_Value to_json(const SqlResultData &sqlResultData, bool rows_as_objects = true);

/*
namespace detail
{
    static void set_from_exception(
        SqlResultData &sqlResultData,
        Opal::Database::DriverException &e)
    {
        sqlResultData.isException = true;
        sqlResultData.errorMessage = e.what();
        sqlResultData.sqlState = e.sqlState();
        sqlResultData.sqlError = e.sqlError();
    }


    static void set_from_exception(
        SqlResultData &sqlResultData,
        Opal::Exception &e)
    {
        sqlResultData.isException = true;
        sqlResultData.errorMessage = e.what();
    }
}
*/

namespace helpers
{
    std::string to_json_string(SqlResultData &&sqlResultData);
}

} // SQL
} // Opal

#endif

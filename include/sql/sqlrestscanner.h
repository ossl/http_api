#ifndef OPAL_SQL_REST_SCANNER_H
#define OPAL_SQL_REST_SCANNER_H

#pragma once

#include "sql/sql.h"
#include "scanner/scanner.h"
#include <list>
#include <set>
#include <map>

namespace Opal::SQL {

class SqlRestScanner
{
public:

    std::map<std::string, Opal::SQL::Token::Type> keywords =
    {
        { "AND"   , Token::Type::And        },
        { "OR"    , Token::Type::Or         },
        { "TRUE"  , Token::Type::True       },
        { "FALSE" , Token::Type::False      },
        { "PLUS"  , Token::Type::Add        },
        { "MINUS" , Token::Type::Subtract   },
        { "DIV"   , Token::Type::Divide     },
        { "MUL"   , Token::Type::Multiply   },
        { "MOD"   , Token::Type::Modulus    }
    };

    std::set<std::string> forbiddenKeywords =
    {
        "SELECT", "INSERT", "DELETE", "UPDATE", "CREATE", "DROP"
    };

    SqlRestScanner(const std::string &line);

    std::list<Token> getTokens() const;

protected:
    void addToken(Token::Type type);
    void addToken(Token::Type type, std::string &&str);
    void scanToken();
    void number();
    void string(char c);
    void identifier();

    Opal::Scanner scanner;

    std::list<Token> tokens;
};

}

#endif

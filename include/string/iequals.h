#ifndef OPAL_STRING_IEQUALS_H
#define OPAL_STRING_IEQUALS_H

#pragma once

#include <string>

namespace Opal {
namespace String {

bool iequals(
    const std::string &a,
    const std::string &b);

bool iequals(
    const std::string &a,
    const std::string &b,
    std::size_t max_length);

} // string
} // Opal

#endif

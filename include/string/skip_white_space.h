#ifndef OPAL_STRING_SKIP_WHITE_SPACE_H
#define OPAL_STRING_SKIP_WHITE_SPACE_H

#pragma once

#include <string>

namespace Opal {
namespace String {

std::size_t skip_white_space(
    const std::string& s, std::size_t start_pos = 0);

} // string
} // Opal

#endif

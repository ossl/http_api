#ifndef OPAL_STRING_TO_UPPER_H
#define OPAL_STRING_TO_UPPER_H

#pragma once

#include <string>

namespace Opal {
namespace String {

void to_upper(std::string &s);

} // string
} // Opal

#endif

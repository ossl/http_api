#ifndef OPAL_STRING_SPLIT_H
#define OPAL_STRING_SPLIT_H

#pragma once

#include "sub_string.h"
#include <string>

namespace Opal {
namespace String {

template <class Container>
Container split(
    const std::string &s,
    const std::string &delimiter,
    std::size_t  start_pos = 0,
    std::size_t *new_pos   = NULL,
    std::size_t  max_size  = std::numeric_limits<std::size_t>::max() )
{
    Container string_container;
        
    while (start_pos < s.length() && string_container.size() < max_size) {
        string_container.push_back(
            sub_string(s, delimiter, start_pos, &start_pos));
    }

    if (new_pos) *new_pos = start_pos; 

    return string_container;
}

} // string
} // Opal

#endif

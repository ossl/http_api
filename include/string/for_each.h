#ifndef OPAL_STRING_FOR_EACH_H
#define OPAL_STRING_FOR_EACH_H

#pragma once

#include <string>
#include <functional>

namespace Opal {
namespace String {

std::size_t for_each (
    const std::string &s,
    const std::string &delimiters,
    std::size_t start_pos,
    std::function<bool(std::string &&sub, char delimiter)> &&receiverFn );

} // string
} // Opal

#endif

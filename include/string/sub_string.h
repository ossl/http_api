#ifndef OPAL_STRING_SUB_STRING_H
#define OPAL_STRING_SUB_STRING_H

#pragma once

#include <string>

namespace Opal {
namespace String {

std::string sub_string_rtrim(
    const std::string &s,
    std::size_t  start_pos = 0,
    std::size_t  amount    = std::string::npos );

std::string sub_string(
    const std::string &s,
    const std::string &delimiter,
    std::size_t  start_pos = 0,
    std::size_t *new_pos   = NULL );

} // string
} // Opal

#endif

#ifndef OPAL_STREAM_TO_STRING_H
#define OPAL_STREAM_TO_STRING_H

#pragma once

#include <streambuf>
#include <string>

namespace Opal {
namespace Stream {

std::string to_string(
    std::streambuf &s, std::size_t max_copy_size);

} // stream
} // Opal

#endif

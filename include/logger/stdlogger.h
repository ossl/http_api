#ifndef OPAL_STD_LOGGER_H
#define OPAL_STD_LOGGER_H

#pragma once

// Local headers
#include "logger.h"

namespace Opal {

class StdLogger : public Logger
{
public:
	StdLogger( const std::string &moduleName,
               const std::string &prefix = std::string() );

	void setPrefix(const std::string &prefix);

	void log(Level level, const std::string &s, ...);
	void info(const std::string &s, ...);
	void warn(const std::string &s, ...);
	void error(const std::string &s, ...);
	void debug(const std::string &s, ...);

	std::string makeLine(const std::string &s);

protected:
	//void log(Level level, const char *fmt, va_list argp);

	void info_p(const std::string &s);
	void warn_p(const std::string &s);
	void error_p(const std::string &s);
	void debug_p(const std::string &s);

	void createString(const std::string &fmt, va_list argp);

	std::string m_moduleName;
	std::string m_prefix;
	std::string m_lineHeader;

	char m_buffer[8 * 1024];

private:
	StdLogger();
};

} // Opal

#endif


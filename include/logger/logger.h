#ifndef OPAL_LOGGER_H
#define OPAL_LOGGER_H

#pragma once

#include <string>

namespace Opal {

class Logger
{
public:
	enum Level { Debug, Info, Warning, Error };

	virtual ~Logger() {}
	virtual void setPrefix(const std::string &prefix) = 0;
	virtual void log(Level level, const std::string &s, ...) = 0;
	virtual void info(const std::string &s, ...) = 0;
	virtual void warn(const std::string &s, ...) = 0;
	virtual void error(const std::string &s, ...) = 0;
	virtual void debug(const std::string &s, ...) = 0;
};

} // Opal

#endif

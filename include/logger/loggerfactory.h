#ifndef OPAL_LOGGER_FACTORY_H
#define OPAL_LOGGER_FACTORY_H

#pragma once

#include <memory>

namespace Opal {

class Logger;
class LoggerFactory
{
public:
    virtual ~LoggerFactory() {}
    virtual std::unique_ptr<Logger>
    createLogger ( const std::string &module,
                   const std::string &prefix ) = 0;
};

} // Opal

#endif

#ifndef OPAL_STD_LOGGER_FACTORY_H
#define OPAL_STD_LOGGER_FACTORY_H

#pragma once

#include "loggerfactory.h"
#include "stdlogger.h"

namespace Opal {

class StdLoggerFactory : public LoggerFactory
{
public:
    std::unique_ptr<Logger>
    createLogger ( const std::string &module,
                   const std::string &prefix )
    {
        return std::unique_ptr<Logger>(new StdLogger(module, prefix));
    }
};

} // Opal

#endif

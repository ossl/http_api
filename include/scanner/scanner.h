#ifndef OPAL_SCANNER_H
#define OPAL_SCANNER_H

#pragma once

#include <string>

namespace Opal {

class Scanner
{
public:
    Scanner(std::string source, size_t start_pos = 0);

    bool at_end();
    char peek();
    char peek_next();
    bool match(char c);
    char advance();
    void mark_position();
    void skip_white_space();
    std::string get_marked_string();

private:
    std::string source;
    std::size_t pos;
    std::size_t marker_pos;
};

} // Opal

#endif

Initial implementation of a HTTP REST API and supporting library.

Current scope:
+ This is an exercise for me to experiment with:
    + Modern C++
    + General design idioms
    + Accessing a IBM DB2 database from C++
    + Combining lambda, async and threading
    + Optimising by means of move semantics
    + Project structuring

+ It is not yet meant for any use beyond that.
+ The code is experimental and not production ready.
+ Eventually it may become part of a larger project where a HTTP REST API
  is required.

The compiled executable will:

+ Start a HTTP REST API server configured to interface with an IBM DB2
  instance.

+ Start a basic HTTP page server that will use a "www" directory
  configured in the same path as the executable.

The HTTP REST API allows:
    * Data to be queried, and a WHERE clause to be encoded in the URL.
    * Stored Procedures to be called with either indexed or named parameters.

Simple initial examples:

1) Returning all data from view or table "foo":

    GET https://<api-url>/api/foo

2) Add "WHERE pk = 50" to the WHERE clause:

    GET https://<api-url>/api/foo?pk=50

3) Add "WHERE pk > 50 AND pk < 100" to the WHERE clause:

    GET https://<api-url>/api/foo?!q=pk>50+and+pk<100

4) Add "WHERE pk LIKE '1%' to the WHERE clause.

    GET https://<api-url>/api/foo?!q=pk+like+'1%'

Expressions are tokenized rather than using substitution to help prevent SQL
injection attacks.  For example, literals can be checked for keywords and
substituted.

Eventually selects and stored procedure calls will be parameterized to help
eliminate the possibility of SQL injection.

More to come.

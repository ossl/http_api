// Local headers
#include "server/server.h"
#include "server/sessionfactory.h"
#include "server/session.h"
#include "server/socket.h"
#include "logger/loggerfactory.h"
#include "logger/logger.h"

// Boost headers
#include <boost/asio.hpp>

// STL headers
#include <iostream>
#include <string>
#include <memory>
#include <forward_list>

namespace Opal {

Server::Server( std::shared_ptr<LoggerFactory> loggerFactory,
                std::shared_ptr<SessionFactory> sessionFactory,
                boost::asio::io_service &io_service,
                const boost::asio::ip::tcp::endpoint &endpoint,
                boost::asio::ssl::context *ssl_context ) :
    io_service(io_service),
    acceptor(io_service, endpoint),
    acceptorRestartTimer(io_service),
    houseKeepingTimer(io_service),
    socket(io_service),
    sessionFactory(sessionFactory),
    loggerFactory(loggerFactory),
    ssl_context(ssl_context)
{
    logger = loggerFactory->createLogger("SERVER", std::to_string(endpoint.port()));
    logger->info("init");
}

void Server::start()
{
    logger->info("start");

    startAccept();
    startHouseKeepingTimer();
}

void Server::stop()
{
    logger->info("stop");

    houseKeepingTimer.cancel();
    acceptorRestartTimer.cancel();
    acceptor.cancel();

    for (auto const &it : sessions) {
        if (!it.expired()) {
            auto session = it.lock();
            if (session)
                session->cancel();
        }
    }
}

void Server::startHouseKeepingTimer()
{
    houseKeepingTimer.expires_from_now(boost::posix_time::minutes(1));
    houseKeepingTimer.async_wait(
        [this, self = shared_from_this()] (const boost::system::error_code &ec) {
            if (ec) return;

            sessions.remove_if([] (auto session) { return session.expired(); });

            startHouseKeepingTimer();
        });
}

void Server::startAccept()
{
    //logger->info("start accept");

    acceptor.async_accept(socket.tcp_socket(),
        [this, self = shared_from_this()] (const boost::system::error_code &ec)
    {
        if (!ec.failed()) {
            //logger->info("accepting connection");

            auto newSession = sessionFactory->createSession(
                std::move(socket), loggerFactory );
            
            sessions.push_front(newSession);

            if (ssl_context)
                newSession->startSSL(*ssl_context);
            else
                newSession->start();
            
            //sessionPending->start();
        } else {
            logger->info("acceptor failed: " + ec.message());

            if (ec != boost::asio::error::operation_aborted) {
                acceptorRestartTimer.expires_from_now(boost::posix_time::seconds(5));
                acceptorRestartTimer.async_wait(
                    [this, self = shared_from_this()] (const boost::system::error_code &ec) {
                        if (!ec) {
                            logger->info("restarting acceptor after timeout.");
                            startAccept();
                        }
                    });
            }

            return;
        }

        startAccept();
    });

    //logger->info("waiting for connection..");
}

} // Opal

#include "server/socket.h"

// Boost headers
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>

// STL headers
#include <functional>
#include <memory>
#include <iostream>

namespace Opal {

Socket::Socket(boost::asio::io_service &io_service)
    : io_service_ref(io_service), socket(io_service)
{}

Socket::Socket(Socket &&other) :
    io_service_ref(other.io_service_ref),
    socket(std::move(other.socket)),
    ssl_stream(std::move(other.ssl_stream))
{}

Socket::~Socket()
{
    close();
}

/*
const boost::asio::io_service & Socket::io_service()
{
    return socket.get_executor();
    //return io_service_ref;
}
*/

bool Socket::isSSL() const
{
    return ssl_stream.get() != nullptr;
}

void Socket::enableSSL(boost::asio::ssl::context &ssl_context)
{
    ssl_stream = std::make_unique<
        boost::asio::ssl::stream<boost::asio::ip::tcp::socket&>>(
            socket, ssl_context);
}

void Socket::cancel()
{
    socket.cancel();
}

boost::asio::ip::tcp::socket& Socket::tcp_socket()
{
    return socket;
}

void Socket::close()
{
    socket.close();
}

} // Opal

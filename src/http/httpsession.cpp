// Local headers
#include "http/httpsession.h"

#include "string/iequals.h"
#include "string/split.h"
#include "string/sub_string.h"
#include "string/to_upper.h"
#include "string/skip_white_space.h"
#include "stream/to_string.h"

namespace
{
    static int DefaultKeepAliveTimeoutSeconds = 60;

    static long numSessions = 0;

    long sessionNo = 0;

    ///
    // Boost ASIO function for matching a newline.
    //
    // Line length is never excessively long for our purposes, so cap
    // it at a reasonable length to help prevent scenarios that could
    // cause us to get stuck forever reading.
    //
	typedef boost::asio::buffers_iterator
        <boost::asio::streambuf::const_buffers_type> iterator;

	static std::pair<iterator, bool> match_newline(iterator begin, iterator end)
	{
		iterator it = begin;
		for (int i = 0; it != end && i < 1024; ++i)
		{
			char ch = *it++;
			if (ch == '\n')
				return std::make_pair(it, true);
		}
		return std::make_pair(it, false);
	}
}

namespace Opal::HTTP {

Session::Session( Socket &&socket,
                  std::shared_ptr<RequestListener> httpRequestListener,
                  std::shared_ptr<LoggerFactory> loggerFactory ) :
    socketTimeoutTimer(
        socket.tcp_socket().get_executor(),
        boost::posix_time::seconds(DefaultTimeoutSeconds)),
    socket(std::move(socket)),
    httpRequestListener(httpRequestListener),
    idleTimeout(DefaultTimeoutSeconds),
    isStarted(false),
    isPersistant(false)
{
    logger = loggerFactory->createLogger("HTTP", "");
}

Session::~Session()
{
    //logger->debug("dtor");
}

bool Session::isOpen()
{
    return socket.tcp_socket().is_open();
}

void Session::cancel()
{
    logger->info("cancel");
    socket.cancel();
}

bool Session::init()
{
    if (isStarted)
    {
        assert(false);
        return true;
    }

    isStarted = true;

    sessionNo = ++numSessions;

    boost::system::error_code ec;
    auto endpoint = socket.tcp_socket().remote_endpoint(ec);
    if (ec)
    {
        logger->warn("failed to get remote endpoint from socket, initialisation stopped.");
        return false;
    }

    peer.ip_address = endpoint.address().to_string();
    peer.port = endpoint.port();

    logger->setPrefix(peer.ip_address + "/" + std::to_string(sessionNo));
    logger->info("connected");

    return true;
}

void Session::start()
{
    if (!init()) return;

    async_read_request_line();
}

void Session::startSSL(boost::asio::ssl::context &ssl_context)
{
    if (!init()) return;
    
    socket.enableSSL(ssl_context);
    socket.handshakeSSL(
        [this, self = shared_from_this()] (const boost::system::error_code &ec)
        {
            //std::cout << "handshake callback.." << std::endl;
            if (ec)
            {
                std::cout << "handshake failed.." << std::endl;
                return;
            }

            async_read_request_line();
        });
}

void Session::interpretRequestHeaders()
{
//    dump_headers();

    isPersistant =
        incoming_request.version == "HTTP/1.1"
            || String::iequals(incoming_request.headers["CONNECTION"], "keep-alive");

    if (isPersistant)
    {
        idleTimeout = DefaultKeepAliveTimeoutSeconds;
    }

    auto it = incoming_request.headers.find("CONTENT-LENGTH");

    try
    {
        incoming_request.contentLength =
            (it == incoming_request.headers.end()) ? 0 : std::stoi(it->second);
    }
    catch (std::exception &e)
    {
        close_with_warning(std::string("malformed header: content-length: ") + e.what());
    }
}

void Session::close_with_warning(const std::string &log_message)
{
    logger->warn(log_message);
    socketTimeoutTimer.cancel();
    socket.close();
}

void Session::async_read_request_line()
{
    resetSocketTimeoutTimer(idleTimeout);

    //logger->info("beginReadRequestLine");

    socket.async_read_until( readBuffer, match_newline,
        [this, self = shared_from_this()] (const boost::system::error_code &ec, std::size_t s)
        {
            if (ec)
            {
                handleASIOError("wait-request", ec);
            }
            else
            {
                auto line = Stream::to_string(readBuffer, s);
                auto lineComponents =
                    String::split<std::vector<std::string>>(
                        line, " ", 0, NULL, 3);

                if (lineComponents.size() != 3)
                {
                    close_with_warning("malformed request");
                }
                else
                {
                    incoming_request.method  = lineComponents.at(0);
                    incoming_request.uri     = lineComponents.at(1);
                    incoming_request.version = lineComponents.at(2);

                    resetSocketTimeoutTimer();

                    async_read_header_line();
                }
            }
        });
}

void Session::async_read_header_line()
{
    socket.async_read_until( readBuffer, match_newline,
        [this, self = shared_from_this()] ( const boost::system::error_code &ec, std::size_t s )
        {
            if (ec)
            {
                handleASIOError("wait-header", ec);
            }
            else
            {
                // If we only read 2 characters or less, it must be a
                // blank line, indicating the end of headers.

                if (s > 2)
                {
                    std::size_t pos;
                    auto line = Stream::to_string(readBuffer, s);
                    auto key  = String::sub_string(line, ":", 0, &pos);

                    if (key.length() == 0)
                    {
                        close_with_warning("bad url param key/value data.");
                    }
                    else
                    {
                        String::to_upper(key);

                        incoming_request.headers[key] = String::sub_string_rtrim(
                            line, String::skip_white_space( line, pos ) );
                
                        async_read_header_line();
                    }
                }
                else
                {
                    readBuffer.consume(s); // Consume newline.
                    
                    interpretRequestHeaders();

                    incoming_request.body.erase();
                    incoming_request.body.reserve(incoming_request.contentLength + 1);
                    
                    async_read_body();
                }
            }
        });
}

void Session::dump_headers()
{
    for (auto const &h : incoming_request.headers)
    {
        logger->debug("hdr: " + h.first + " = " + h.second);
    }
}

void Session::async_read_body()
{
    // Nothing left to read, so queue the request and start again.

    if (incoming_request.contentLength == 0)
    {
        queueCurrentRequest();

        if (isPersistant)
        {
            async_read_request_line();
        }

        return;
    }

    // First see if we already have some data in the buffer, and if
    // so we want to extract that first.

    std::size_t s = readBuffer.size();

    if (s)
    {
        flushReadBufferToBody(s);
        return;
    }

    resetSocketTimeoutTimer();

    // Perform the async receive in chunks to prevent the idle timer
    // kicking in.

    socket.async_read( readBuffer,
        boost::asio::transfer_exactly(
            incoming_request.contentLength > 1024
                ? 1024
                : incoming_request.contentLength),
        [this, self = shared_from_this()] (const boost::system::error_code &ec, std::size_t s)
        {
            if (ec)
            {
                handleASIOError("wait-body", ec);
            }
            else
            {
                flushReadBufferToBody(s);
            }
        });
}

void Session::flushReadBufferToBody(std::size_t s)
{
    assert(s > 0);

    // Read the buffer in chunks using a buffer.

    while (s)
    {
        char buf[256];
        auto copySize = readBuffer.sgetn(buf, s > sizeof(buf) ? sizeof(buf) : s);
        s -= copySize;
        incoming_request.body.append(buf, copySize);
        incoming_request.contentLength -= copySize;
    }

    async_read_body();
}

void Session::queueCurrentRequest()
{
    // We can safely "move" the transient request info here as
    // the session is done with it.

    httpRequestQueue.push(Request(
        std::move(incoming_request.version),
        peer.ip_address,
        peer.port,
        std::move(incoming_request.method),
        std::move(incoming_request.uri),
        std::move(incoming_request.body),
        std::move(incoming_request.headers)));

    // Start async queue processing if needed.

    if (httpRequestQueue.size() == 1)
    {
        beginProcessRequestQueue();
    }
}

void Session::beginProcessRequestQueue()
{
    if (httpRequestQueue.size() == 0)
    {
        return;
    }

    httpRequestListener->onRequest(
        httpRequestQueue.front(),
        [this, self = shared_from_this()] (Response &&httpResponse)
        {
            sendResponse(
                std::move(httpRequestQueue.front()),
                std::move(httpResponse));

            httpRequestQueue.pop();
            
            beginProcessRequestQueue();
        });
}

void Session::sendResponse(Request &&request, Response &&response)
{
    assert(response.httpResponseCode <= 999);

    char tmp[32];
    snprintf(tmp, sizeof(tmp), " %03d ", response.httpResponseCode);

    writeBufferAppend(std::move(request.httpVersion));
    writeBufferAppend(tmp);
    writeBufferAppendLine(Error::toString(response.httpResponseCode));
    writeBufferAppendLine("Server: Opal-rest-api");
    writeBufferAppendLine("Content-Length: " + std::to_string(response.body.size()));
    writeBufferAppendLine("Access-Control-Allow-Origin: *");
    writeBufferAppendLine("Access-Control-Allow-Methods: POST, GET, OPTIONS, DELETE, PUT");
    writeBufferAppendLine("Access-Control-Allow-Headers: origin, x-requested-with, content-type, authorization");

    for (auto header : response.headers)
    {
        writeBufferAppendLine(header.first + ": " + header.second);
    }

    // RFC states if client sends a keep-alive, we should send one back.

    if (String::iequals(request.headers["CONNECTION"], "keep-alive"))
    {
        writeBufferAppendLine("Connection: keep-alive");
    }

    writeBufferAppendLine();

    if (response.body.length())
    {
        writeBufferAppend(std::move(response.body));
    }
    
    writeBufferFlush();
}

void Session::writeBufferAppendLine()
{
    writeBufferAppend("\r\n");
}

void Session::writeBufferAppendLine(std::string &&line)
{
    writeBufferAppend(line + "\r\n");
}

void Session::writeBufferAppend(std::string &&message)
{
    writeBuffer += message;
}

void Session::writeBufferFlush()
{
    if (writeBuffer.length() == 0) {
        return;
    }

    socket.async_write(
        boost::asio::buffer(writeBuffer, writeBuffer.length()),
        [this, self = shared_from_this()] (const boost::system::error_code &ec, std::size_t s)
        {
            if (ec)
            {
                handleASIOError("wait-write", ec);
            }

            if (!isPersistant)
            {
                socketTimeoutTimer.cancel();
            }
        });

    writeBuffer.clear();
}

void Session::logError(const char *opName, const boost::system::error_code &ec)
{
    if (ec != boost::asio::error::operation_aborted)
    {
        logger->error("[%s] %s: %s",
            opName, ec.category().name(), ec.message().c_str());
    }
    else
    {
        logger->info("[%s] interrupted.", opName);
    }
}

void Session::resetSocketTimeoutTimer(int seconds)
{
    //logger->info("resetting socket timer: %d seconds", seconds);
    socketTimeoutTimer.expires_from_now(boost::posix_time::seconds(seconds));
    socketTimeoutTimer.async_wait(
        [this, self = shared_from_this()] (const boost::system::error_code &ec)
        {
            if (ec) return;
            logger->info("timer: cancelling socket operations..");
            socket.cancel();
        });
}

void Session::handleASIOError(const char *opName, const boost::system::error_code& ec)
{
    httpRequestListener->cancel();

    socketTimeoutTimer.cancel();

    ///
    // The type of error determines how we handle closing the
    // socket.
    //
    // If a cancellation has occurred, it is likely the socket
    // is still active, so we can attempt to shutdown each
    // layer properly, such as the SSL layer.
    //
    // If the peer has closed the connection (eof) or an actual
    // error has occurred, it is likely the socket is not
    // useable, so we may as well just close the tcp connection.
    //
    if (ec == boost::asio::error::operation_aborted)
    {
        close();
    }
    else if (ec == boost::asio::error::eof)
    {
        socket.close();
    }
    else
    {
        logger->debug("socket is in an unknown state; closing (" + ec.message() + ")");

        socket.close();
    }
}

void Session::close()
{
    if (socket.isSSL())
    {
        logger->debug("socket is ssl, sending shutdown..");

        ///
        // SSL shutdown needs a timeout:
        //  - Handle broken socket
        //  - Handle misbehaving clients
        //
        resetSocketTimeoutTimer(5);

        socket.shutdownSSL([this, self = shared_from_this()] (const boost::system::error_code &ec) {
            socketTimeoutTimer.cancel();
            //logger->debug("SSL shutdown: " + ec.message());
            //logger->debug("SSL shutdown: Closing Socket.");
            socket.close();
        });
    }
    else
    {
        //logger->debug("Closing Socket.");

        socket.close();
    }
}

} // Opal::HTTP

#include "http/httprequestlistener.h"

namespace Opal::HTTP {

RequestListener::RequestListener()
{}

void RequestListener::onRequest(
    Request &httpRequest, ResponseHandler &&responseHandler)
{
    auto findIt = methodFunctionMap.find(httpRequest.method);
    if (findIt != methodFunctionMap.end())
    {
        findIt->second( httpRequest, std::move(responseHandler) );
    }
    else
    {
        Response httpResponse(200);
        httpResponse.headers["Content-Type"] = "text";
        httpResponse.body = "$ Hello " + httpRequest.sourceIpAddress + " from HTTP API\r\n";
        httpResponse.body += "> You sent me this: " + httpRequest.uri + "\r\n";
        httpResponse.body += "! I'm not sure what it means; perhaps the request is malformed?\r\n";

        responseHandler(std::move(httpResponse));
    }
}

void RequestListener::cancel()
{}

void RequestListener::async_listen(
    const std::vector<std::string> &methods,
    std::function<void(Request&, ResponseHandler&&)> &&handler)
{
    for (auto const &method : methods)
    {
        methodFunctionMap[method] = handler;
    }
}

void RequestListener::sendOptions(ResponseHandler &&handler)
{
    Response httpResponse(204);
    handler(std::move(httpResponse));
}

} // Opal::HTTP

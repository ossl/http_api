#include "http/httpserver.h"
#include "http/httpsessionfactory.h"
#include "http/httprequestlistener.h"

namespace Opal::HTTP {

Server::Server( std::shared_ptr<LoggerFactory> loggerFactory,
                boost::asio::io_service &io_service,
		        const boost::asio::ip::tcp::endpoint &endpoint,
                boost::asio::ssl::context *ssl_context )
{
    httpRequestListener = std::make_shared<RequestListener>();

    server = std::make_shared<Opal::Server>(
        loggerFactory,
        std::make_shared<SessionFactory>(httpRequestListener),
        io_service,
        endpoint,
        ssl_context);
}

void Server::start()
{
    server->start();
}

void Server::cancel()
{
    server->stop();
}

void Server::async_listen(
    const std::vector<std::string> &methods,
    std::function<void(Request&, ResponseHandler&&)> &&handler)
{
    httpRequestListener->async_listen(methods, std::move(handler));
}

}

// Local headers
#include "logger/stdlogger.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

#include <time.h>

namespace Opal {

StdLogger::StdLogger( const std::string &moduleName,
                      const std::string &prefix) :
	m_moduleName(moduleName),
    m_prefix(prefix)
{}

void StdLogger::setPrefix(const std::string &prefix)
{
	m_prefix = prefix;
}

void StdLogger::log(Level level, const std::string &fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);

    createString(fmt, ap);

	switch (level)
	{
	case Info:
		info_p(m_buffer);
		break;
	case Error:
		error_p(m_buffer);
		break;
	case Warning:
		warn_p(m_buffer);
		break;
	case Debug:
		debug_p(m_buffer);
		break;
	}

	va_end(ap);
}

///
//  Create a string based on va args.
//
void StdLogger::createString(const std::string &fmt, va_list argp)
{
	vsnprintf(m_buffer, sizeof(m_buffer), fmt.c_str(), argp);
}

///
//  Info
//
void StdLogger::info(const std::string &fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	
	createString(fmt, ap);

	va_end(ap);

	info_p(m_buffer);
}

void StdLogger::info_p(const std::string &s)
{
	printf(makeLine(s).c_str());
}

///
//  Warn
//
void StdLogger::warn(const std::string &fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);

	createString(fmt, ap);

	va_end(ap);

	warn_p(m_buffer);
}

void StdLogger::warn_p(const std::string &s)
{
	printf(makeLine(s).c_str());
}

//
// Error
//
void StdLogger::error(const std::string &fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);
	
	createString(fmt, ap);

	va_end(ap);

	error_p(m_buffer);
}

void StdLogger::error_p(const std::string &s)
{
	printf(makeLine(s).c_str());
}

///
// Debug
//
void StdLogger::debug(const std::string &fmt, ...)
{
	va_list ap;
	va_start(ap, fmt);

	createString(fmt, ap);

	va_end(ap);

	debug_p(m_buffer);
}

void StdLogger::debug_p(const std::string &s)
{
	printf(makeLine(s).c_str());
}

//
//
//
std::string StdLogger::makeLine(const std::string &s)
{
	char buf[96];

    const time_t t = time(NULL);
    int n = strftime(buf, sizeof(buf), "%c ", localtime(&t));
    //int n = 0;
    if (m_prefix.length())
		snprintf(&buf[n], sizeof(buf) - n, "%-8s %s ", m_moduleName.c_str(), m_prefix.c_str());
	else
		snprintf(&buf[n], sizeof(buf) - n, "%-8s ", m_moduleName.c_str());
    
	return std::string(buf) + s + "\r\n";
}

} // Opal

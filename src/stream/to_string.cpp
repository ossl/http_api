#include "stream/to_string.h"

namespace Opal::Stream {

std::string to_string(
    std::streambuf &s, std::size_t max_copy_size)
{
    std::string new_string;
    new_string.resize(max_copy_size);
    s.sgetn(&new_string[0], max_copy_size);
    return new_string;
}

} // Opal::Stream

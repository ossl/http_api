#include "string/skip_white_space.h"

#include <stdexcept>

namespace Opal::String {

std::size_t skip_white_space(
    const std::string &s, std::size_t start_pos)
{
    if (start_pos > s.size())
    {
        throw std::out_of_range("start_pos > size");
    }

    auto raw_start = s.c_str();
    auto raw_pos = raw_start + start_pos;

    while (*raw_pos && isspace(*raw_pos))
    {
        ++raw_pos;
    }

    return raw_pos - raw_start;
}

} // Opal::String

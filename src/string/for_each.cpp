#include "string/for_each.h"
#include "string/skip_white_space.h"
#include "string/sub_string.h"

#include <functional>
#include <algorithm>

namespace Opal::String {

std::size_t for_each (
    const std::string &s,
    const std::string &delimiters,
    std::size_t start_pos,
    std::function<bool(std::string &&sub, char delimiter)> &&receiverFn)
{
    while (start_pos < s.size())
    {
        start_pos = skip_white_space(s, start_pos);
    
        auto delim_pos = s.find_first_of( delimiters, start_pos );

        if ( delim_pos == s.npos || delim_pos == s.size() )
        {
            receiverFn(
                sub_string_rtrim(
                    s, start_pos, s.size() - start_pos), 0);

            start_pos = delim_pos;

            break;
        }
        
        if ( !receiverFn(
            sub_string_rtrim(
                s, start_pos, delim_pos - start_pos), s.at(delim_pos)))
        {
            start_pos = delim_pos;
            break;
        }

        start_pos = delim_pos + 1;
    }

    return start_pos;
}

} // Opal::String

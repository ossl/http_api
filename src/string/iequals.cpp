#include "string/iequals.h"
#include <string>

namespace Opal::String {

bool iequals(const std::string &a, const std::string &b)
{
    std::size_t alen = a.length();
    if (alen != b.length())
        return false;

    auto raw_a = a.c_str();
    auto raw_b = b.c_str();

    for (std::size_t i = 0; i < alen; ++i) {
        if (std::toupper(raw_a[i]) != std::toupper(raw_b[i])) {
            return false;
        }
    }

    return true;
}

bool iequals(const std::string &a, const std::string &b, std::size_t max_len)
{
    std::size_t alen = a.length();
    std::size_t blen = b.length();

    if (alen > max_len) alen = max_len;
    if (blen > max_len) blen = max_len;

    if (alen != blen) return false;

    auto raw_a = a.c_str();
    auto raw_b = b.c_str();

    for (std::size_t i = 0; i < alen; ++i) {
        if (std::toupper(raw_a[i]) != std::toupper(raw_b[i])) {
            return false;
        }
    }

    return true;
}

} // Opal::String


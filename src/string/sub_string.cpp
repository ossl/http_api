#include "string/sub_string.h"
#include "string/skip_white_space.h"

#include <stdexcept>

namespace Opal::String {

std::string sub_string(
    const std::string &source,
    const std::string &delimiter,
    std::size_t  start_pos,
    std::size_t *new_pos)
{
    std::string new_string;
    std::size_t parse_start_pos = skip_white_space(source, start_pos);

	if (parse_start_pos < source.size()) {
    	std::size_t parse_end_pos = source.find(
            delimiter, parse_start_pos);

    	if (parse_end_pos == source.npos)
        {
    		parse_end_pos = source.length();
            start_pos = parse_end_pos;
        }
        else
        {
	        start_pos = parse_end_pos + delimiter.length();
	    }

    	new_string = sub_string_rtrim(
            source, parse_start_pos, parse_end_pos - parse_start_pos);
    }

    if (new_pos) *new_pos = start_pos;

	return new_string;
}

std::string sub_string_rtrim(
    const std::string &s,
    std::size_t start_pos,
    std::size_t amount )
{
    if (start_pos > s.size())
    {
        throw std::out_of_range("start_pos > size");
    }

    if (start_pos + amount < start_pos || start_pos + amount > s.size())
    {
        amount = s.size() - start_pos;
    }

    auto raw_str = s.c_str() + start_pos;

    while (amount && isspace(raw_str[amount - 1]))
    {
        --amount;
    }

    return s.substr(start_pos, amount);
}

} // Opal::String

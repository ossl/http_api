#include "string/to_upper.h"

#include <algorithm>

namespace Opal::String {

void to_upper(std::string &s)
{
    std::transform(s.begin(), s.end(), s.begin(),
        [](unsigned char c){ return std::toupper(c); });
}

} // Opal::String

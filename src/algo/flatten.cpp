#include "algo/detail/flatten_impl.h"

namespace Opal::algo::detail {

std::string flatten(const std::string &item, const std::string &separator)
{
    return item;
}

std::string flatten(int i, const std::string &separator)
{
    return std::to_string(i);
}

std::string flatten(float f, const std::string &separator)
{
    return std::to_string(f);
}

std::string flatten(std::size_t s, const std::string &separator)
{
    return std::to_string(s);
}

}

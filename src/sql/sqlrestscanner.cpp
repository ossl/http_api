#include "sql/sqlrestscanner.h"
#include "string/to_upper.h"

namespace Opal::SQL {

SqlRestScanner::SqlRestScanner(const std::string &line) :
    scanner(line)
{
    while ( !scanner.at_end() ) {
        scanner.mark_position();
        scanToken();
        scanner.skip_white_space();
    }
}

std::list<Token> SqlRestScanner::getTokens() const
{
    return this->tokens;
}

void SqlRestScanner::addToken(Token::Type type)
{
    tokens.push_back(Token(type));
}

void SqlRestScanner::addToken(Token::Type type, std::string &&str)
{
    tokens.push_back(Token(type, std::move(str)));
}

void SqlRestScanner::scanToken()
{
    char c = scanner.advance();

    if (c == 0)
        return;

    if (std::isspace(c))
        return;

    //printf("scanToken: %c\r\n", c);

    switch (c) {
        case '(': addToken(Token::Type::OpenParen); break;
        case ')': addToken(Token::Type::CloseParen); break;
        case '{': addToken(Token::Type::OpenBrace); break;
        case '}': addToken(Token::Type::CloseBrace); break;
        case '-': addToken(Token::Type::Subtract); break;
        case '+': addToken(Token::Type::Add); break;
        case '*': addToken(Token::Type::Multiply); break;
        case '/': addToken(Token::Type::Divide); break;
        case '=': addToken(Token::Type::Equal); break;
        case '!': addToken(scanner.match('=') ? Token::Type::NotEqual : Token::Type::Not); break;
        case '<': addToken(scanner.match('=') ? Token::Type::LessOrEqual : Token::Type::Less); break;
        case '>': addToken(scanner.match('=') ? Token::Type::MoreOrEqual : Token::Type::More); break;
        default:
            if (std::isdigit(c))
                number();
            else if (c == '_' || std::isalpha(c))
                identifier();
            else if (c == '"' || c == '\'')
                string(c);
            else
                printf("Unexpected character.\r\n");
    }
}

void SqlRestScanner::number()
{
    while (std::isdigit(scanner.peek())) {
        scanner.advance();
    }

    if (scanner.peek() == '.' && std::isdigit(scanner.peek_next())) {
        scanner.advance();

        while (std::isdigit(scanner.peek())) {
            scanner.advance();
        }
    }

    addToken(Token::Type::Number, scanner.get_marked_string());
}

void SqlRestScanner::string(char c) {
    while (scanner.peek() != c && !scanner.at_end()) {
        scanner.advance();
    }

    if (scanner.at_end())
    {
        printf("Unterminated string.\r\n");
        return;
    }

    scanner.advance();

    addToken(Token::Type::String, scanner.get_marked_string());
}

void SqlRestScanner::identifier()
{
    while (std::isalnum(scanner.peek()))
        scanner.advance();

    std::string identString = scanner.get_marked_string();

    Opal::String::to_upper(identString);

    // Decide if the identifier is really a keyword.

    auto keywordIter = keywords.find(identString);
    if ( keywordIter != keywords.end() ) {
        addToken(keywordIter->second, std::move(identString));
    } else if ( forbiddenKeywords.find(identString) != forbiddenKeywords.end() ) {
        addToken(Token::Type::Ident, "_" + identString + "_");
    } else {    
        addToken(Token::Type::Ident, std::move(identString));
    }
}

} // Opal::SQL


#include "sql/sqlresultdata.h"
#include "string/sub_string.h"

namespace Opal::SQL {

JSON::JSON_Value to_json(const SqlResultData &sqlResultData, bool rows_as_objects)
{
    JSON::JSON_Value json;
    if (sqlResultData.isException)
    {
        json.object["is-error"] = JSON::Literal::True;
        json.object["sql-state"] = sqlResultData.sqlState;
        json.object["sql-error"] = sqlResultData.sqlError;
        json.object["error-msg"] = sqlResultData.errorMessage;
    }
    else
    {
        json.object["is-error"] = JSON::Literal::False;
        json.object["cols"] = sqlResultData.columns;

        if (rows_as_objects)
        {
            JSON::JSON_Array row_object_array;
            for (const auto &row : sqlResultData.rows)
            {
                JSON::JSON_Object json_row_object;
                int col_index = 0;
                for (const auto &col : sqlResultData.columns)
                {    
                    json_row_object[col] = row.at(col_index++);
                }
                row_object_array.push_back(json_row_object);
            }
            json.object["rows"] = row_object_array;
        }
        else
        {
            json.object["rows"] = sqlResultData.rows;
        }    
    }
    
    return json;
}

/*

std::string helpers::to_json_string(SqlResultData &&sqlResultData)
{
	std::string json = "{\r\n";

    //json += "    \"sql_state\": \"" + sqlResultData.errorMessage + "\"\r\n";

    if (sqlResultData.isException)
    {
        json += "    \"error\": \"" + sqlResultData.errorMessage + "\",\r\n";
        json += "    \"sql-state\": \"" + sqlResultData.sqlState + "\",\r\n";
        json += "    \"sql-error\": \"" + sqlResultData.sqlError + "\"\r\n";
    }
    else
    {
	    json += "    \"cols\": [";

	    std::string separator;
	    for (auto const &col : sqlResultData.columns)
	    {
		    json += separator + "\"" + col + "\"";
    		separator = ", ";
	    }

	    json += "],\r\n";
	
        if (sqlResultData.rows.size())
        {
            json += "    \"rows\": [\r\n";

	        separator = "";
	        for (auto const &row : sqlResultData.rows)
	        {
    		    json += separator;
		        json += "        [";
		        separator = "";
	
        	    for (auto const &d : row)
		        {
    			    json += separator;
			        json += "\"" + String::sub_string_rtrim(d) + "\"";
    			    separator = ", ";
		        }

		        json += "]";
                separator = ",\r\n";
    	    }

            json += "\r\n";
        	json += "    ]\r\n";
        }
        else
        {
            json += "    \"rows\": []\r\n";
        }
        

    }

	json += "}\r\n";

	return json;
}
*/

} // Opal

#include "sql/sqlrestinterpretter.h"
#include "sql/sqlrestscanner.h"
#include "sql/sqlquery.h"

#include "string/split.h"
#include "http/http.h"
#include "http/uri.h"

//#include <boost/property_tree/ptree.hpp>
//#include <boost/property_tree/json_parser.hpp>

#include <iostream>
#include <sstream>

#include <assert.h>

namespace Opal::SQL {

SqlRestInterpretter::SqlRestInterpretter()
{}

SqlCall SqlRestInterpretter::makeCall(const HTTP::Request &request)
{
    if ( request.method != Opal::HTTP::method_post &&
         request.method != Opal::HTTP::method_put )
    {
        assert(false);
    }

    //std::cout << "SqlRestInterpretter::makeCall" << std::endl;

    SqlCall sqlCall;

    HTTP::URI uri(request.uri);

    // Stored Procedure Name

    if (uri.path.size())
    {
        sqlCall.stored_proc = uri.path.front();
    }

    // Body contains parameters

    auto contentTypeIt = request.headers.find("CONTENT-TYPE");

    if (contentTypeIt != request.headers.end())
    {
        std::string contentType = contentTypeIt->second;
        if (contentType == "text")
        {
            
        }
        else if (contentType == "application/json")
        {
            
        }
    }

    return sqlCall;
}

SqlQuery SqlRestInterpretter::makeQuery(const HTTP::Request &httpRequest)
{
    if ( httpRequest.method != Opal::HTTP::method_get )
    {
        assert(false);
    }

    //std::cout << "SqlRestInterpretter::makeQuery" << std::endl;

    SqlQuery sqlQuery;

    HTTP::URI uri(httpRequest.uri);

    std::string fieldList;

    {
	    auto it = uri.query.find("!f");
		if (it != uri.query.end()) {
            sqlQuery.select = String::split<std::list<std::string>>(it->second, ",");
        }
	}

    // View we are selecting from.

    if (uri.path.size())
    {
        sqlQuery.from = uri.path.front();
    }

    //bool whereStarted = false;

    {
		for (auto it = uri.query.begin(); it != uri.query.end(); ++it)
        {
    	    if (it->first.length() && it->first.at(0) != '!' && it->first.at(0) != '$')
            {
                if (sqlQuery.where.size() > 0)
                {
                    sqlQuery.where.push_back(Token(Token::Type::And));
                }

                SqlRestScanner scanner(it->first + "='" + it->second + "'");
                auto tokens = scanner.getTokens();

                sqlQuery.where.push_back(Token(Token::Type::OpenParen));
                sqlQuery.where.splice(sqlQuery.where.end(), tokens);
                sqlQuery.where.push_back(Token(Token::Type::CloseParen));
            }
    	}
	}

    if (sqlQuery.where.size() > 0)
    {
        sqlQuery.where.push_front(Token(Token::Type::OpenParen));
        sqlQuery.where.push_back(Token(Token::Type::CloseParen));
    }

    {
        auto it = uri.query.find("!q");
	    if (it != uri.query.end())
	    {
            if (sqlQuery.where.size() > 0)
            {
                sqlQuery.where.push_back(Token(Token::Type::And));                
            }

            SqlRestScanner scanner(it->second);
            auto tokens = scanner.getTokens();

            sqlQuery.where.push_front(Token(Token::Type::OpenParen));
            sqlQuery.where.splice(sqlQuery.where.end(), tokens);
            sqlQuery.where.push_back(Token(Token::Type::CloseParen));
	    }
	}

    // Group by

    {
	    auto it = uri.query.find("!group_by");
    	if (it != uri.query.end())
        {
            sqlQuery.groupBy = String::split<std::list<std::string>>(it->second, ",");
        }
	}

	// Order by

    {
	    auto it = uri.query.find("!order_by");
	    if (it != uri.query.end())
        {
            sqlQuery.orderBy = String::split<std::list<std::string>>(it->second, ",");
        }
    }

    // Offset

    {
		auto it = uri.query.find("!offset");
		if (it != uri.query.end())
		{
            sqlQuery.offset = atoi(it->second.c_str());
		}
	}

	// Limit

    {
		auto it = uri.query.find("!limit");
		if (it != uri.query.end())
		{
            sqlQuery.limit = atoi(it->second.c_str());
		}
	}

    return sqlQuery;
}

} // Opal::SQL

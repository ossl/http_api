#include "scanner/scanner.h"
#include "string/skip_white_space.h"
#include <string>
#include <assert.h>

namespace Opal {

Scanner::Scanner(std::string source, size_t start_pos) :
    source(std::move(source)), pos(start_pos), marker_pos(start_pos)
{
    assert(start_pos <= source.size());
}

std::string Scanner::get_marked_string()
{
    assert(pos >= marker_pos);
    return source.substr(marker_pos, pos - marker_pos);
}

void Scanner::mark_position()
{
    marker_pos = pos;
}

void Scanner::skip_white_space()
{
    pos = String::skip_white_space(source, pos);
}

bool Scanner::at_end()
{
    return pos >= source.size();
}

char Scanner::peek()
{
    if (at_end())
        return 0;

    return source.at(pos);
}

char Scanner::peek_next()
{
    if (pos + 1 >= source.length())
        return 0;

    return source.at(pos + 1);
}

bool Scanner::match(char c)
{
    return peek() == c;
}

char Scanner::advance()
{
    assert(pos < source.size());
    return source.at(pos++);
}

} // Opal

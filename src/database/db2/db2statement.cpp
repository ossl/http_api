///
//  Lightweight IBM DB2 Driver wrapper
//  Opal Software Solutions Limited (C) 2020
//  Mark Simonetti
//  22.06.2020
//

#include "database/db2/db2statement.h"
#include "database/exceptions.h"

namespace Opal::Database {

Db2Statement::Db2Statement(SQLHANDLE hndConn)
{
    SQLRETURN ret = SQLAllocHandle(SQL_HANDLE_STMT, hndConn, &hndStat);
    
    if (ret != SQL_SUCCESS)
    {
        throw DriverException(
            DB2::detail::get_diagnostic_records(hndConn, SQL_HANDLE_DBC));
    }
}

Db2Statement::~Db2Statement()
{
    if (hndStat != SQL_INVALID_HANDLE)
    {
        SQLFreeHandle(SQL_HANDLE_STMT, hndStat);
    }
}

void Db2Statement::exec(const std::string &sql)
{
    lastQuery = sql;
    
    SQLRETURN ret = SQLExecDirect(hndStat, (SQLCHAR *) sql.c_str(), SQL_NTS);
    
    if (ret != SQL_SUCCESS && ret != SQL_STILL_EXECUTING)
    {
        throw DriverException(diagnosticData());
    }
}

Db2ResultSet Db2Statement::query(const std::string &sql)
{
    exec(sql);
    return Db2ResultSet(hndStat);
}

bool Db2Statement::diagnosticData(detail::DiagnosticData &diagnosticData, int index)
{
    return SQL_SUCCEEDED(
        DB2::detail::get_diagnostic_record(diagnosticData, index, hndStat, SQL_HANDLE_STMT));
}

std::vector<detail::DiagnosticData> Db2Statement::diagnosticData()
{
    return DB2::detail::get_diagnostic_records(hndStat, SQL_HANDLE_STMT);
}

} // Opal::db

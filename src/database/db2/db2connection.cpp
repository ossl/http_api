///
//  Lightweight IBM DB2 Driver wrapper
//  Opal Software Solutions Limited (C) 2020
//  Mark Simonetti
//  22.06.2020
//

#include "database/db2/db2connection.h"
#include "database/exceptions.h"
#include <assert.h>

namespace Opal::Database {

Db2Connection::Db2Connection(
    const std::string &server,
    int port,
    const std::string &username,
    const std::string &password,
    const std::string &db)
{
    std::string connectionString = "{IBM DB2 ODBC Driver}";
    
    connectionString += ";Database=" + db;
    connectionString += ";Hostname=" + server;
    connectionString += ";Port=" + std::to_string(port);
    connectionString += ";UID=" + username;
    connectionString += ";PWD=" + password;

    SQLRETURN sqlReturn;

    if ((sqlReturn = SQLAllocHandle(SQL_HANDLE_ENV, SQL_NULL_HANDLE, &hndEnv)) != SQL_SUCCESS)
    {
        throw AllocEnvironmentException();
    }

    if ((sqlReturn = SQLSetEnvAttr(hndEnv, SQL_ATTR_ODBC_VERSION, (SQLPOINTER)SQL_OV_ODBC3, 0)) != SQL_SUCCESS)
    {
        throw DriverException(
            DB2::detail::get_diagnostic_records(hndEnv, SQL_HANDLE_ENV));
    }

    if ((sqlReturn = SQLAllocHandle(SQL_HANDLE_DBC, hndEnv, &hndConn)) != SQL_SUCCESS)
    {
        throw DriverException(
            DB2::detail::get_diagnostic_records(hndEnv, SQL_HANDLE_ENV));
    }
    
    SQLSMALLINT outLen = 0;
    char szOut[256];

    if ((sqlReturn = SQLDriverConnect(
        hndConn, NULL,
        (SQLCHAR *)connectionString.c_str(), connectionString.length(),
        (SQLCHAR *)szOut, sizeof(szOut) - 1, &outLen,
        SQL_DRIVER_NOPROMPT)) != SQL_SUCCESS)
    {
        throw DriverException(
            DB2::detail::get_diagnostic_records(hndConn, SQL_HANDLE_DBC));
    }
}

Db2Connection::~Db2Connection()
{
    if (hndConn != SQL_INVALID_HANDLE)
    {
        SQLDisconnect(hndConn);
        SQLFreeHandle(SQL_HANDLE_DBC, hndConn);
    }

    if (hndConn != SQL_INVALID_HANDLE)
    {
        SQLFreeHandle(SQL_HANDLE_ENV, hndEnv);
    }
}

Db2Statement Db2Connection::createStatement()
{
    return Db2Statement(hndConn);
}

bool Db2Connection::diagnosticData(detail::DiagnosticData &diagnosticData, int index)
{
    return SQL_SUCCEEDED(
        DB2::detail::get_diagnostic_record(diagnosticData, index, hndConn, SQL_HANDLE_DBC));
}

std::vector<detail::DiagnosticData> Db2Connection::diagnosticData()
{
    return DB2::detail::get_diagnostic_records(hndConn, SQL_HANDLE_DBC);
}

} // Opal::db

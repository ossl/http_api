#include "database/db2/detail/get_diagnostic_records.h"
#include "string/sub_string.h"

#include <iostream>

namespace Opal::Database::DB2::detail {

SQLRETURN get_diagnostic_record(
    Opal::Database::detail::DiagnosticData &diagnosticData,
    SQLSMALLINT index,
    SQLHANDLE   handle,
    SQLSMALLINT type)
{
    SQLCHAR state[8];
    SQLCHAR text[256];
    SQLRETURN ret;
    
    ret = SQLGetDiagRec(type, handle, index, state, NULL, text, sizeof(text), NULL );

    if (SQL_SUCCEEDED(ret))
    {
        diagnosticData.sqlState = (char *)state;
        diagnosticData.message = String::sub_string_rtrim((char *)text);
    }
    
    return ret;
}

std::vector<Opal::Database::detail::DiagnosticData> get_diagnostic_records(
    SQLHANDLE   handle,
    SQLSMALLINT type)
{
    SQLRETURN ret;
    SQLSMALLINT i = 0;
    
    std::vector<Opal::Database::detail::DiagnosticData> records;

    do
    {
        Opal::Database::detail::DiagnosticData diagnosticData;

        ret = get_diagnostic_record(diagnosticData, ++i, handle, type);

        if (SQL_SUCCEEDED(ret))
        {
            records.push_back(std::move(diagnosticData));
        }
    } while( ret == SQL_SUCCESS );

    return records;
}

} // Opal::Database::detail


///
//  Lightweight IBM DB2 Driver wrapper
//  Opal Software Solutions Limited (C) 2020
//  Mark Simonetti
//  22.06.2020
//

#include "database/db2/db2resultset.h"
#include "database/db2/detail/get_diagnostic_records.h"
#include "database/exceptions.h"
#include "string/to_upper.h"
#include "string/sub_string.h"
#include <cstring>
#include <algorithm>

namespace Opal::Database {

Db2ResultSet::Db2ResultSet(SQLHANDLE hndStat) : hndStat(hndStat)
{
    loadColInfo();
}

std::size_t Db2ResultSet::numCols()
{
    SQLSMALLINT numCols;
    SQLRETURN ret = SQLNumResultCols(hndStat, &numCols);

    if (ret != SQL_SUCCESS)
    {
        throw DriverException(diagnosticData());
    }

    return numCols;
}

std::string Db2ResultSet::data(std::size_t columnIdx, bool trim)
{
    if (fetchCount == 0)
    {
        throw NoDataException();
    }

    if (columnIdx < 1 || columnIdx > numCols())
    {
        throw NoSuchColumnIndexException(columnIdx);
    }

    char *p = (char *)rowInfo[columnIdx - 1].data;

    return trim ? String::sub_string_rtrim(p) : p;
}

std::string Db2ResultSet::data(const std::string &columnName, bool trim)
{
    // FIXME
    std::string tmpColumnName = columnName;
    String::to_upper(tmpColumnName);
    auto it = fieldMap.find(tmpColumnName);
    if (it == fieldMap.end())
    {
        throw NoSuchColumnNameException(columnName);
    }

    return data(it->second, trim);
}

std::string Db2ResultSet::columnName(std::size_t columnIdx)
{
    if (columnIdx < 1 || columnIdx > numCols())
    {
        throw NoSuchColumnIndexException(columnIdx);
    }

    return (char *) colInfo[columnIdx - 1].columnName;    
}

std::map<std::string, std::size_t> Db2ResultSet::columnNameMap()
{
    return fieldMap;
}

std::vector<std::string> Db2ResultSet::columnNames()
{
    std::vector<std::string> columnNames(fieldMap.size());
    for (auto const &e : fieldMap)
    {
        columnNames[e.second - 1] = e.first;
    }

    return columnNames;
}

/*
std::set<std::string> Db2ResultSet::columnNames()
{
    std::set<std::string> theSet;

    for (auto const& column : fieldMap)
    {
        theSet.insert(column.first);
    }

    return theSet;
}
*/

bool Db2ResultSet::next(bool dumpToOutput)
{
    return fetch(dumpToOutput);
}

bool Db2ResultSet::fetch(bool dumpToOutput)
{
    SQLRETURN ret = SQLFetch(hndStat);

    if (ret == SQL_NO_DATA)
    {
        return false;
    }

    if (!SQL_SUCCEEDED(ret))
    {
        throw DriverException(diagnosticData());
    }

    ++fetchCount;

    if (dumpToOutput)
    {
        for (std::size_t i = 0; i < numCols(); ++i)
        {
            printf("row=%lu col=%lu name=%s size=%d scale=%d disp_size=%d ret_len=%d data=%s\r\n",
                rowCount,
                i + 1,
                colInfo[i].columnName,
                colInfo[i].columnSize,
                colInfo[i].columnScale,
                colInfo[i].columnDisplaySize,
                rowInfo[i].returnLength,
                (char*)rowInfo[i].data);
        }
    }

    ++rowCount;

    return true;
}

// MAS: Needs normalising.  Just rough to begin with.

void Db2ResultSet::loadColInfo()
{
    if (colInfo) delete colInfo;


    auto nCols = numCols();

    colInfo = new ColInfo[nCols];

    for (std::size_t i = 0; i < nCols; ++i)
    {
        colInfo[i].columnName[0] = 0;
        colInfo[i].columnNameLength = 0;
        colInfo[i].columnType = 0;
        colInfo[i].columnSize = 0;
        colInfo[i].isNullable = 0;
        colInfo[i].columnDisplaySize = 0;
    }

    if (rowInfo) delete rowInfo;

    rowInfo = new RowInfo[nCols];

    for (size_t i = 0; i < nCols; ++i)
    {
        if (SQLDescribeCol(
                hndStat,
                (SQLSMALLINT)(i + 1),
                colInfo[i].columnName,
                sizeof(colInfo[i].columnName),
                &colInfo[i].columnNameLength,
                &colInfo[i].columnType,
                &colInfo[i].columnSize,
                &colInfo[i].columnScale,
                &colInfo[i].isNullable) != SQL_SUCCESS)
        {
            throw DriverException(diagnosticData());
        }

        fieldMap.insert(std::pair<std::string, std::size_t>(
            (char *)colInfo[i].columnName, i + 1));

        if (SQLColAttribute(hndStat,
                ( SQLSMALLINT ) ( i + 1 ),
                SQL_DESC_DISPLAY_SIZE,
                NULL,
                0,
                NULL,
                &colInfo[i].columnDisplaySize ) != SQL_SUCCESS)
        {
            throw DriverException(diagnosticData());
        }
     
        rowInfo[i].bufferLength = colInfo[i].columnDisplaySize + 1;
        rowInfo[i].data = new SQLCHAR[rowInfo[i].bufferLength + 1];
        
        memset(rowInfo[i].data, 0, rowInfo[i].bufferLength + 1);

        switch(colInfo[i].columnType )
        {
            case SQL_BLOB:
            case SQL_CLOB:
                if (SQLBindCol(hndStat,
                        ( SQLSMALLINT ) ( i + 1 ),
                        SQL_C_BINARY,
                        rowInfo[i].data,
                        rowInfo[i].bufferLength,
                        &rowInfo[i].returnLength ) != SQL_SUCCESS)
                {
                    throw DriverException(diagnosticData());
                }

                break;

            default:
                if (SQLBindCol(hndStat,
                    ( SQLSMALLINT ) ( i + 1 ),
                    SQL_C_CHAR,
                    rowInfo[i].data,
                    rowInfo[i].bufferLength,
                    &rowInfo[i].returnLength ) != SQL_SUCCESS)
                {
                    throw DriverException(diagnosticData());
                }

                break;
        }
    }
}

void Db2ResultSet::loadRowInfo()
{
    
}

bool Db2ResultSet::diagnosticData(detail::DiagnosticData &diagnosticData, int index)
{
    return SQL_SUCCEEDED(
        DB2::detail::get_diagnostic_record(diagnosticData, index, hndStat, SQL_HANDLE_STMT));
}

std::vector<detail::DiagnosticData> Db2ResultSet::diagnosticData()
{
    return DB2::detail::get_diagnostic_records(hndStat, SQL_HANDLE_STMT);
}

} // Opal::db

#include "json/json.h"

namespace Opal::JSON {

std::string escape(const std::string &in)
{
    std::string out;
    out.reserve(in.size() * 2);
    for (auto c : in)
    {
        switch (c)
        {
            case '"': out += "\\\""; break;
            case '\f': out += "\\f"; break;
            case '\b': out += "\\b"; break;
            case '\n': out += "\\n"; break;
            case '\r': out += "\\r"; break;
            case '\t': out += "\\t"; break;
            case '\\': out += "\\\\"; break;
            default:
                out += c;
        }
    }
    return out;
}

}
